{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Notebook Intent\n",
    "\n",
    "There's a direct relationship between the time-domain impulse response, and the frequency-domain transfer function.  Either of the forms can be used to analyze the behaviors of the filter, and we can choose one or the other depending on what we need to know or show.  This notebook attempts to demonstrate the relationship.\n",
    "\n",
    "For copyright and license information, see footnote.\n",
    "\n",
    "# Required Resources\n",
    "\n",
    "We need [ThinkDSP]() for signal generation, `scipy` for filter design and convolution, and support code (`transfer_function_plots`) for displays:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import thinkdsp as dsp\n",
    "from scipy import signal\n",
    "import transfer_function_plots as disp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Basic Theory\n",
    "\n",
    "[Euler's Formula](https://en.wikipedia.org/wiki/Euler%27s_formula) provides a relationship between the complex exponential and trigonometric functions, specifically\n",
    "\n",
    "$$e^{j\\theta} = \\cos(\\theta) + j\\sin(\\theta)$$\n",
    "\n",
    "In the DSP context, the angle is a function of time, or sample count, so that\n",
    "\n",
    "$$e^{j\\omega n} = \\cos(\\omega n) + j\\sin(\\omega n)$$\n",
    "\n",
    "and therefore we can eliminate the imaginary part by summing a positive and negative exponential\n",
    "\n",
    "$$\\begin{align}\n",
    "e^{j\\omega n} + e^{-j\\omega n} &= \\cos(\\omega n) + j\\sin(\\omega n) + \\cos(\\omega n) - j\\sin(\\omega n)\\\\\n",
    "& = 2\\cos(\\omega n)\n",
    "\\end{align}$$\n",
    "\n",
    "so that we can write\n",
    "\n",
    "$$\\cos(\\omega n) = \\frac{1}{2}\\left(e^{j\\omega n} + e^{-j\\omega n}\\right)$$\n",
    "\n",
    "Writing the cosine function (and therefore any signal that can be represented as a linear weighted sum of cosines - anything that has a Fourier series representation) as a complex exponential is important because the complex exponential has a particular property for all linear, time-invariant (LTI) systems (i.e., all filters of interest).  Specifically, they are *eigenfunctions* of LTI systems: they pass through the filter without change in frequency, just a change in magnitude,\n",
    "\n",
    "$$y(n) = x(n)\\otimes h(n)\\;\\text{and}\\;x(n) = e^{j\\omega n} \\Rightarrow y(n) = H(\\omega)x(n)$$\n",
    "\n",
    "for some complex number $H(\\omega)$ that is a function solely of the frequency of $x(n)$ and the properties of the filter.  Note, of course, that if $H(\\omega)$ is a complex number, this can both scale, and phase shift the input.\n",
    "\n",
    "We can examine this behavior by running signals through a generic filter.  First, the signals:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "signal_freq = (400, 800)\n",
    "samp_freq = 8000\n",
    "\n",
    "input_samples = []\n",
    "for s in signal_freq:\n",
    "    input_signal = dsp.CosSignal(freq=s, amp=1.0, offset=0.0)\n",
    "    input_samples.append(input_signal.make_wave(duration=1.0, framerate=samp_freq))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, the filter, of arbitrary length, and corner frequency:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "filter_length = 51\n",
    "corner_freq = 600\n",
    "\n",
    "filt_b = signal.firwin(filter_length, corner_freq, window='blackmanharris', fs=samp_freq)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For each signal, we can apply the same filter, and catch the outputs for display:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "output_samples = []\n",
    "for samples in input_samples:\n",
    "    output_samples.append(samples.convolve(filt_b))\n",
    "    \n",
    "disp.display_timeplot(input_samples, output_samples, trange=(0.01, 0.02))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Clearly, the output signal is the same frequency as the input signal in each case, but is both shifted in time and amplified (in this case with less than unit gain, as might be expected given that the filter is low-pass and the signals straddle the corner frequency).  This is exactly the behavior we'd expect from an eigenfunction (or, in this case, a pair of eigenfunctions).\n",
    "\n",
    "The $H(\\omega)$ is different for each case here, and in general it's different for each frequency.  We could tabulate the results, but computing the response as a function of all frequencies is much more useful.  This can be done directly from the coefficients of the filter (for FIR or IIR) by using the `scipy.signal.freqz()` routine, which computes the frequency and response functions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f, H = signal.freqz(filt_b, fs=samp_freq)\n",
    "disp.display_transfer_function(f, H)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is called the *transfer function*, and is usually interpreted as the frequency response of the filter.  The name comes from the observation that in the frequency domain, we can compute the output of the filter $Y(\\omega)$ from the input $X(\\omega)$ and the transfer function as $Y(\\omega) = X(\\omega)H(\\omega)$: the function *transfers* the input to output.  Importantly, a simple rearrangement,\n",
    "\n",
    "$$H(\\omega) = \\frac{Y(\\omega)}{X(\\omega)}$$\n",
    "\n",
    "shows that the transfer function is also the ratio of output to input response.\n",
    "\n",
    "# Impulse Response and Transfer Function Relationship\n",
    "\n",
    "The impulse response is a full characterization of the filter in the time domain, and the transfer function is a full characterization in the frequency domain; it seems likely, therefore, that they should be related somehow.  In fact, you can show that they are a Fourier Transform pair:\n",
    "\n",
    "$$\n",
    "\\begin{align}\n",
    "H(\\omega) &= \\mathcal{F}(h(n))\\\\\n",
    "h(n) &= \\mathcal{F}^{-1}(H(\\omega))\n",
    "\\end{align}\n",
    "$$\n",
    "\n",
    "which is another way of generating FIR filters (i.e., design $H(\\omega)$ and then inverse transform).\n",
    "\n",
    "Logically, if the impulse and transfer functions are related in this way, computing the filter effect in either domain should be possible.  This is in fact a standard property of the Fourier Transform known as the Convolution Theorem:\n",
    "\n",
    "$$y(n) = x(n)\\otimes h(n) \\Leftrightarrow Y(\\omega) = X(\\omega)H(\\omega)$$\n",
    "\n",
    "or, \"convolution in time means multiplication in frequency, and vice versa\".  This observation leads to the potential for accelerating computation of filters by working in the frequency domain rather than in time, although there are some important caveats in getting this right (see `FastConvolution` notebook for more details).\n",
    "\n",
    "# Copyright and License\n",
    "\n",
    "Copyright 2019, University of New Hampshire, Center for Coastal and Ocean Mapping.\n",
    "\n",
    "This program is free software: you can redistribute it and/or modify\n",
    "it under the terms of the GNU General Public License as published by\n",
    "the Free Software Foundation, either version 3 of the License, or\n",
    "(at your option) any later version.\n",
    "\n",
    "This program is distributed in the hope that it will be useful,\n",
    "but WITHOUT ANY WARRANTY; without even the implied warranty of\n",
    "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n",
    "GNU General Public License for more details.\n",
    "\n",
    "You should have received [a copy of the GNU General Public License](COPYING)\n",
    "along with this program.  If not, see [here](https://www.gnu.org/licenses/)."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
