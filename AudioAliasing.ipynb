{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Notebook Intent\n",
    "\n",
    "This noteboook provides another way to look at aliasing by constructing a series of simple audio tones, and combinations of tones, at different sampling rates.  The goal is to teach an intuitive understanding of the [Nyquist-Shannon Sampling Theorem](https://en.wikipedia.org/wiki/Nyquist%E2%80%93Shannon_sampling_theorem).\n",
    "\n",
    "For copyright and license information, see footnote.\n",
    "\n",
    "# Required Resources\n",
    "\n",
    "We need a simple support set (`dsp_aux`) to make the various signals of interest, and MatplotLib as a means to make plots of the results as required."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import dsp_aux as aux\n",
    "import audio_aliasing_plots as disp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Simple Notes\n",
    "\n",
    "Start by generating a simple pair of notes at 200Hz and 1800Hz, sampling at 8000Hz, and then make audio of the results:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "low_freq = 200\n",
    "med_freq = 3800\n",
    "fs = 8000\n",
    "\n",
    "low_note = aux.make_note(low_freq, fs)\n",
    "high_note = aux.make_note(med_freq, fs)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(note that we need to generate these separately, since you can only have one audio output per cell evaluated)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "disp.display_audio(low_note, trange=(0, 0.05))\n",
    "low_note.make_audio()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "disp.display_audio(high_note, trange=(0, 0.05))\n",
    "high_note.make_audio()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Anomalous High Frequency Representation\n",
    "\n",
    "If we continue to attempt to make higher frequencies, however, things start to go wrong (as we've been seeing):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "high_freq = 7800\n",
    "\n",
    "highest_note = aux.make_note(high_freq, fs)\n",
    "highest_note.make_audio()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Instead of a very high frequency, what we hear instead is something that's remarkably similar to the 200Hz signal that we heard before: play them back to back, and see if you can tell the difference.\n",
    "\n",
    "# Pairs of Notes\n",
    "\n",
    "Consider making a pair of notes at the same time, specifically at 200Hz, and 7800Hz:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pair1 = aux.make_chord((low_freq, high_freq), fs)\n",
    "pair1.make_audio(norm=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That's odd, but looking at the signal itself tells the story:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "disp.display_audio(pair1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It appears that all we have left from the combination of the two notes is noise (magnitude order $10^{-12}$).\n",
    "\n",
    "Try that again with a different pair, at 200Hz and 8200Hz (notice that it's OK to have a frequency higher than the sampling frequency, although it's a little odd in practice):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pair2 = aux.make_chord((low_freq, fs+200), fs)\n",
    "disp.display_audio(pair2, trange=(0,0.05))\n",
    "pair2.make_audio()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Again, that's a little odd: instead of two notes at different frequencies, we have a single note instead, and at 200Hz.  You can also check that 8200Hz sounds just like 200Hz.\n",
    "\n",
    "# Summary of Responses\n",
    "\n",
    "What we've found so far is:\n",
    "- Signal at 200Hz plays like 200Hz\n",
    "- Signal at 3800Hz plays like 3800Hz\n",
    "- Signal at 7800Hz plays like 200Hz\n",
    "- Signal at 8200Hz plays like 200Hz\n",
    "- Signal at (200 + 7800) Hz plays silence\n",
    "- Signal at (200 + 8200) Hz plays like 200Hz\n",
    "\n",
    "On the face of it, this doesn't make much sense.  However, if instead of thinking of frequency as a linear scale we think of it as a circle, we can start to interpret the effects.  This interpretation is usually known as \"circular frequency\", and comes about as a requirement of digitial sampling of any kind (which is what causes aliasing in the first place).\n",
    "\n",
    "Imagine that you take the frequency axis that extends from $-f_s/2$ to $f_s/2$ and then bend it into a circle so that $\\pm f_s/2$ are adjacent.  Then, if you start at zero and move around the circle towards positive frequencies, eventually you'll loop over $f_s/2$ and head into negative frequency range.  But you're still heading in the positive direction, so the only consistent interpretation is that the negative frequency that you're at has to be equivalent to the positive frequency you moved around to.  You can play the same argument in the negative direction: if you start from zero and head in the negative direction, eventually you'll loop past $-f_s/2$ and head into the positive frequency axis - but again, it has to be the same signal, so the interpretation has to be that the high negative frequency is equivalent to a positive frequency.\n",
    "\n",
    "If you carry the experiment far enough, you'll get back to the zero mark from the other direction.  Again, the only consistent interpretation is that high frequency signals close to the sampling frequency have to be equiavlent to low negative frequencies (and high negative frequencies close to $-f_s$ are equivalent to low positive frequencies).\n",
    "\n",
    "This explains what we observed:\n",
    "- 7800Hz is 200Hz below the sampling frequency, and is therefore equivalent to -200Hz. Because our ears can't sense phase (-200Hz is 200Hz playing backwards, or 180 degrees out of phase), it sounds like 200Hz.\n",
    "- 8200Hz is 200Hz above the sampling frequency, and is therefore equivalent to 200Hz (i.e., one full loop round the circle, then another 200Hz).\n",
    "- Since 7800Hz is equivalent to -200Hz, playing it and 200Hz together results in silence: the signals cancel out exactly.  (This is the basis of noise cancelling headphones, and a key technology in most long-range telecommunications including acoustic and cable modems.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "disp.display_audio(low_note, trange=(0, 0.05), overplot=highest_note)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Since 8200Hz is equivalent to 200Hz, playing it together with 200Hz just results in the same signal twice, so all you hear is 200Hz.\n",
    "\n",
    "Understanding these gives us some important constraints:\n",
    "1. You can't tell the difference, once sampled, between $f_0$ and $f_s + f_0$, or even $2f_s + f_0$.  That is, there are frequencies that you can't distinguish, and if they're both present in the input signal, Bad Things will ensue.  This is called **aliasing**.\n",
    "2. If we want to avoid this problem, we need to ensure that the sampling frequency is **at least** twice the highest frequency in the input signal, or, equivalently, that the highest input signal frequency is less than half the sampling frequency.  This is known as the [Nyquist-Shannon Sampling Theorem](https://en.wikipedia.org/wiki/Nyquist%E2%80%93Shannon_sampling_theorem).\n",
    "\n",
    "The frequency at which we loop round from positive to negative (or vice versa) is important enough that it gets its own name: the [Nyquist Frequency](https://en.wikipedia.org/wiki/Nyquist_frequency), named after [Harry Nyquist](https://en.wikipedia.org/wiki/Harry_Nyquist).  Signals being captured from the real world generally don't have a hard deadline on their frequency content (although they generally do fall off with frequency), so it can be difficult to ensure that there are no frequencies that would cause aliasing.  In most cases, however, we can either:\n",
    "- Filter the signal before sampling in order to enforce the constraint (known as \"anti-aliasing\").\n",
    "- Set the sampling frequency sufficiently high that the input signal spectrum is sufficiently decayed before the Nyquist frequency so as to avoid problems.\n",
    "\n",
    "Each of these approaches has advantages and disadvantages; which you choose is application specific.\n",
    "\n",
    "# Copyright and License\n",
    "\n",
    "Copyright 2019, University of New Hampshire, Center for Coastal and Ocean Mapping.\n",
    "\n",
    "This program is free software: you can redistribute it and/or modify\n",
    "it under the terms of the GNU General Public License as published by\n",
    "the Free Software Foundation, either version 3 of the License, or\n",
    "(at your option) any later version.\n",
    "\n",
    "This program is distributed in the hope that it will be useful,\n",
    "but WITHOUT ANY WARRANTY; without even the implied warranty of\n",
    "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n",
    "GNU General Public License for more details.\n",
    "\n",
    "You should have received [a copy of the GNU General Public License](COPYING)\n",
    "along with this program.  If not, see [here](https://www.gnu.org/licenses/)."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
