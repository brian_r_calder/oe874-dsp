#   Copyright 2019, University of New Hampshire, Center for Coastal and Ocean Mapping.
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

import matplotlib.pyplot as plt
import numpy as np
import dsp_aux as aux


class BadParams(Exception):
    pass


def display_timeplot(input_samp, output_samp, **kwargs):
    plt.figure(figsize=(14,10))

    if len(input_samp) is not len(output_samp):
        raise BadParams()

    n_subplots = len(input_samp)
    for s in range(n_subplots):
        plt.subplot(n_subplots, 1, s+1)
        plt.plot(input_samp[s].ts, input_samp[s].ys, output_samp[s].ts, output_samp[s].ys)
        plt.grid()
        plt.xlabel('Time (s)')
        plt.ylabel('Signal Amplitude')
        plt.legend(('Input Signal', 'Output Signal'), loc='upper right')
        if 'trange' in kwargs:
            plt.gca().set(xlim=kwargs['trange'])

    if 'save' in kwargs:
        aux.save_figure(kwargs['save'])


def display_transfer_function(f, h, **kwargs):
    plt.figure(figsize=(14,10))
    if 'frange' in kwargs:
        xrange=kwargs['frange']
    else:
        xrange=(0, f[-1])

    plt.subplot(2, 1, 1)
    plt.plot(f, 20.0*np.log10(abs(h)))
    plt.grid()
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Magnitude (dB)')
    plt.gca().set(xlim=xrange)

    plt.subplot(2, 1, 2)
    plt.plot(f, np.unwrap(np.angle(h)))
    plt.grid()
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Phase (radians)')
    plt.gca().set(xlim=xrange)

    if 'save' in kwargs:
        aux.save_figure(kwargs['save'])
