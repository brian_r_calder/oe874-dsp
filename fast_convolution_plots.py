#   Copyright 2019, University of New Hampshire, Center for Coastal and Ocean Mapping.
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

import matplotlib.pyplot as plt
import dsp_aux as aux
from scipy import signal
import numpy as np


def display_start_point(pure, noise, **kwargs):
    plt.figure(figsize=(14, 10))

    plt.subplot(2, 1, 1)
    plt.plot(pure.ts, pure.ys, noise.ts, noise.ys)
    plt.xlim([0, 0.05])
    plt.grid()
    plt.xlabel('Time (s)')
    plt.ylabel('Amplitude')
    plt.legend(('Original Signal', 'Noisy Signal'))

    signal_f, signal_psd = signal.periodogram(pure.ys, pure.framerate)
    noise_f, noise_psd = signal.periodogram(noise.ys, noise.framerate)

    plt.subplot(2, 1, 2)
    plt.plot(signal_f, 20.0 * np.log10(abs(signal_psd)), noise_f, 20.0 * np.log10(noise_psd))
    plt.ylim([-140, 0])
    plt.xlim([0, 1000])
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('PSD (Power/Hz, dB)')
    plt.grid()
    plt.legend(('Original Signal', 'Noisy Signal'))

    if 'save' in kwargs:
        aux.save_figure(kwargs['save'])

    plt.show()


def display_padded(impulse_resp, original, linear_filt, fast_filt, **kwargs):
    plt.figure(figsize=(14, 10))

    plt.subplot(3, 1, 1)
    plt.stem(impulse_resp.ys, use_line_collection=True)
    plt.xlabel('Sample Number')
    plt.ylabel('Impulse Response Coefficient')
    plt.grid()
    plt.axis((0, 102, -0.05, 0.05))

    plt.subplot(3, 1, 2)

    filt_freq, filt_trans = signal.freqz(impulse_resp.ys, fs=impulse_resp.framerate)

    plt.plot(filt_freq, 20.0 * np.log10(abs(filt_trans)))
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Magnitude')
    plt.axis((0, 1000, -60, 5))
    plt.grid()

    plt.subplot(3, 1, 3)
    plt.plot(original.ts, original.ys, fast_filt.ts, fast_filt.ys, 'gs', linear_filt.ts, linear_filt.ys, 'k.-',
             linear_filt.ts[:8000], linear_filt.ys[:8000] - fast_filt.ys[:8000])
    plt.grid()
    plt.axis((0.0, 0.01, -2.0, 2.0))
    plt.legend(['Noisy Signal', 'Fast Filtered', 'Linear Filtered', 'Difference'], loc='upper right')
    plt.xlabel('Time (s)')
    plt.ylabel('Amplitude')

    if 'save' in kwargs:
        aux.save_figure(kwargs['save'])

    plt.show()


def display_error_signal(original, linear_filt, fast_filt_padded, fast_filt_unpadded, **kwargs):
    def stdaxis():
        plt.xlabel('Time (s)')
        plt.ylabel('Amplitude')
        plt.grid()

    plt.figure(figsize=(14, 10))

    plt.subplot(3, 1, 1)
    plt.plot(original.ts, original.ys, fast_filt_padded.ts, fast_filt_padded.ys, 'cs', linear_filt.ts, linear_filt.ys,
             'k.-')
    plt.axis([0, 0.01, -2.0, 2.0])
    plt.legend(['Noisy Signal', 'Padded Fast Filtered', 'Linear Filtered'], loc='upper right')
    stdaxis()

    plt.subplot(3, 1, 2)
    plt.plot(original.ts, original.ys, fast_filt_unpadded.ts, fast_filt_unpadded.ys, 'rs-', linear_filt.ts,
             linear_filt.ys, 'k.-')
    plt.axis([0, 0.01, -2.0, 2.0])
    plt.legend(['Noisy Signal', 'Unpadded Fast Filtered', 'Linear Filtered'], loc='upper right')
    stdaxis()

    plt.subplot(3, 1, 3)
    t = linear_filt.ts[:8000]
    padded_diff = linear_filt.ys[:8000] - fast_filt_padded.ys[:8000]
    unpadded_diff = linear_filt.ys[:8000] - fast_filt_unpadded.ys[:8000]
    plt.plot(t, padded_diff, 'c', t, unpadded_diff, 'r')
    plt.axis([0, 0.01, -1.2, 1.2])
    plt.legend(['Error, Padded', 'Error, Unpadded'], loc='upper right')
    stdaxis()

    if 'save' in kwargs:
        aux.save_figure(kwargs['save'])

    plt.show()
