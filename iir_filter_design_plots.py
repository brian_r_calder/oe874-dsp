#   Copyright 2019, University of New Hampshire, Center for Coastal and Ocean Mapping.
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

import matplotlib.pyplot as plt
import numpy as np
from scipy import signal
import dsp_aux as aux


class BadParams(Exception):
    pass


def display_iir_filters(b, a, sampling_frequency, **kwargs):
    def treat_axes(plotnum, n_subplots, min_amp, max_amp):
        if plotnum == (n_subplots - 1):
            plt.xlabel('Coefficient Number')
        plt.ylabel('Coefficient Value')
        plt.grid()
        plt.gca().set(ylim=(min_amp, max_amp))

    period = 1.0 / sampling_frequency
    n_subplots = len(b)
    if len(a) is not len(b):
        raise BadParams

    # Go through the filters, and find the maximum/minimum amplitude, and time
    min_amp = 0.0
    max_amp = 0.0
    for f in range(n_subplots):
        min_a = a[f].min()
        if min_a < min_amp:
            min_amp = min_a

        min_b = b[f].min()
        if min_b < min_amp:
            min_amp = min_b

        max_a = a[f].max()
        if max_a > max_amp:
            max_amp = max_a

        max_b = b[f].max()
        if max_b > max_amp:
            max_amp = max_b

    # Generate a stack of N plots, all on the same axes for comparison
    plt.figure(figsize=(14, 10))
    for f in range(n_subplots):
        plt.subplot(n_subplots, 1, f + 1)

        plt.plot(range(len(a[f])), a[f], 'o-', range(len(b[f])), b[f], 'o-')
        plt.legend(('Feedback Coefficients', 'Input Coefficients'))
        if 'title' in kwargs:
            plt.title(kwargs['title'][f])
        treat_axes(f, n_subplots, 1.1 * min_amp, 1.1 * max_amp)

    # Save if the user requested an output
    if 'save' in kwargs:
        aux.save_figure(kwargs['save'])


def display_iir_spectra(b, a, sampling_frequency, **kwargs):
    def treat_axes(ax, sampling_frequency, filter_length, plotnum, **kwargs):
        ax.set_xlabel('Frequency (Hz)')
        if 'ylabel' in kwargs:
            ax.set_ylabel(kwargs['ylabel'])
        else:
            ax.set_ylabel('Filter Amplitude (dB)')
        if 'legend' in kwargs:
            ax.legend((kwargs['legend'][plotnum],))
        else:
            label = 'N = %d' % (filter_length,)
            ax.legend((label,))

        ax.grid()
        ax.set(xlim=(0, sampling_frequency / 2.0))
        if 'linrange' in kwargs:
            ax.set(ylim=kwargs['linrange'])
        else:
            if 'logrange' in kwargs:
                ax.set(ylim=kwargs['logrange'])
            else:
                ax.set(ylim=(-40, 5))

    n_subplots = len(b)
    plt.figure(figsize=(14, 10))
    for f in range(n_subplots):
        fs, hs = signal.freqz(b[f], a=a[f], fs=sampling_frequency)
        plt.subplot(n_subplots, 1, f + 1)
        plt.plot(fs, abs(hs), 'k')
        treat_axes(plt.gca(), sampling_frequency, len(b[f]), f, ylabel='Filter Amplitude', linrange=(-0.1, 1.2),
                   **kwargs)

    if 'save' in kwargs:
        aux.save_figure(kwargs['save'])

    plt.figure(figsize=(14, 10))
    for f in range(n_subplots):
        fs, hs = signal.freqz(b[f], a=a[f], fs=sampling_frequency)
        plt.subplot(n_subplots, 1, f + 1)
        plt.plot(fs, 20.0 * np.log10(abs(hs)), 'k')
        treat_axes(plt.gca(), sampling_frequency, len(b[f]), f, **kwargs)

    if 'save' in kwargs:
        aux.save_figure(kwargs['save'])


def display_spectra_comparison(b, a, sampling_frequency, legend_text, **kwargs):
    def treat_axes(ax, ylabel, yrange, legend_text):
        ax.set_xlabel('Frequency (Hz)')
        ax.set_ylabel(ylabel)
        if yrange is not None:
            ax.set(ylim=yrange)
        ax.grid()
        ax.legend(legend_text)

    n_filters = len(b)
    if len(a) is not len(b):
        raise BadParams

    fs = []
    hs = []
    for f in range(n_filters):
        freq, trans = signal.freqz(b[f], a=a[f], fs=sampling_frequency)
        fs.append(freq)
        hs.append(trans)

    plt.figure(figsize=(14, 10))
    # Magnitudes
    plt.subplot(2, 1, 1)
    for f in range(n_filters):
        plt.plot(fs[f], 20.0 * np.log10(abs(hs[f])))

    if 'logrange' in kwargs:
        ylimit = kwargs['logrange']
    else:
        ylimit = (-40, 5)

    treat_axes(plt.gca(), 'Magnitude', ylimit, legend_text)

    # Phases
    plt.subplot(2, 1, 2)
    for f in range(n_filters):
        plt.plot(fs[f], np.unwrap(np.angle(hs[f])))

    treat_axes(plt.gca(), 'Phase (rad)', None, legend_text)

    if 'save' in kwargs:
        aux.save_figure(kwargs['save'])


def display_paired_spectra_comparison(filters, sampling_frequency, legend_text, **kwargs):
    def treat_axes(ax, ylabel, yrange, legend_text):
        ax.set_xlabel('Frequency (Hz)')
        ax.set_ylabel(ylabel)
        if yrange is not None:
            ax.set(ylim=yrange)
        ax.grid()
        ax.legend(legend_text)

    n_filters = len(filters)

    fs = []
    hs = []
    for f in range(n_filters):
        freq, trans = signal.freqz_zpk(filters[f][0], filters[f][1], filters[f][2], fs=sampling_frequency)
        fs.append(freq)
        hs.append(trans)

    if 'logrange' in kwargs:
        yrange = kwargs['logrange']
    else:
        yrange = (-40, 5)

    n_subplots = int(n_filters / 2)
    plt.figure(figsize=(14, 10))
    for s in range(n_subplots):
        plt.subplot(n_subplots, 1, s + 1)
        start_plot = s * 2
        plt.plot(fs[start_plot], 20.0 * np.log10(abs(hs[start_plot])))
        plt.plot(fs[start_plot + 1], 20.0 * np.log10(abs(hs[start_plot + 1])))
        labels = legend_text[start_plot:start_plot + 2]
        treat_axes(plt.gca(), 'Magnitude (dB)', yrange, labels)

    if 'save' in kwargs:
        aux.save_figure(kwargs['save'])


def display_wave_comparison(ts, ys, names, **kwargs):
    n_signals = len(ys)

    plt.figure(figsize=(14, 10))

    for s in range(n_signals):
        plt.plot(ts, ys[s], 'o-')

    plt.xlabel('Time (s)')
    plt.ylabel('Signal Amplitude')
    plt.grid()
    plt.gca().set(xlim=(0, ts[100]))
    plt.legend(names)

    if 'save' in kwargs:
        aux.save_figure(kwargs['save'])


def show_spectrogram(wave, **kwargs):
    plt.figure(figsize=(14, 10))
    plt.specgram(wave.ys, NFFT=512, noverlap=384, Fs=wave.framerate, scale='dB', vmin=-150, vmax=-40)
    plt.jet()
    plt.colorbar()
    plt.xlabel('Time (s)')
    plt.ylabel('Frequency (Hz)')
    plt.grid()

    if 'save' in kwargs:
        aux.save_figure(kwargs['save'])


def display_filter_comparison(filters, sampling_frequency, legend_text, **kwargs):
    def treat_axes(ax, ylabel, yrange, legend_text):
        ax.set_xlabel('Frequency (Hz)')
        ax.set_ylabel(ylabel)
        if yrange is not None:
            ax.set(ylim=yrange)
        ax.grid()
        ax.legend(legend_text)

    n_filters = len(filters)

    fs = []
    hs = []
    for f in range(n_filters):
        freq, trans = signal.freqz_zpk(filters[f][0], filters[f][1], filters[f][2], fs=sampling_frequency)
        fs.append(freq)
        hs.append(trans)

    plt.figure(figsize=(14, 10))
    # Magnitudes
    plt.subplot(2, 1, 1)
    for f in range(n_filters):
        plt.plot(fs[f], 20.0 * np.log10(abs(hs[f])))

    if 'logrange' in kwargs:
        ylimit = kwargs['logrange']
    else:
        ylimit = (-40, 5)

    treat_axes(plt.gca(), 'Magnitude', ylimit, legend_text)

    # Phases
    plt.subplot(2, 1, 2)
    for f in range(n_filters):
        plt.plot(fs[f], np.unwrap(np.angle(hs[f])))

    treat_axes(plt.gca(), 'Phase (rad)', None, legend_text)

    if 'save' in kwargs:
        aux.save_figure(kwargs['save'])
