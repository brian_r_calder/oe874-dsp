#   Copyright 2019, University of New Hampshire, Center for Coastal and Ocean Mapping.
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

import thinkdsp as dsp
import dsp_aux as aux
import numpy as np
import matplotlib.pyplot as plt


def sinc_interpolate(wave, new_framerate):
    """Generate a sinc-interpolated version of the given sampled signal at the
    specified framerate.  This is theoretically the optimal reconstruction of the
    signal, so long as the sinc-pulse is truly infinite.  Because we're working with
    a finite representation, it's only approximate.

    In:
        wave:           ThinkDSP.Wave object to interpolate
        new_framerate:  Sampling frequency for the interpolated output

    Out:
        ThinkDSP.Wave object for the interpolated signal.
    """
    n_samp = int((wave.end - wave.start)*new_framerate)
    ts = np.linspace(wave.start, wave.end, n_samp)
    ys = np.zeros(n_samp)
    for s in range(n_samp):
        for j in range(len(wave.ts)):
            ys[s] += wave.ys[j] * np.sinc(wave.framerate*(ts[s] - wave.ts[j]))
    return dsp.Wave(ys, ts=ts, framerate=new_framerate)


def display_signals(full, sampled, **kwargs):
    def treat_axes(plot, n_subplots, **kwargs):
        if plot == n_subplots-2:
            plt.xlabel('Time (s)')
        plt.ylabel('Amplitude')
        if 'title' in kwargs:
            plt.title(kwargs['title'])
        plt.grid()

    if 'overplot' in kwargs:
        overplot = kwargs['overplot']
    else:
        overplot = None

    plt.figure(figsize=(14,10))
    n_subplots = len(sampled) + 1

    plt.subplot(n_subplots, 1, 1)
    plt.plot(full.ts, full.ys)
    treat_axes(1, n_subplots, title='Sampled: %.1f kHz' % (full.framerate / 1000.0,))

    for s in range(len(sampled)):
        plt.subplot(n_subplots, 1, s + 2)
        plt.stem(sampled[s].ts, sampled[s].ys, use_line_collection=True)
        if overplot is not None:
            plt.plot(overplot[s].ts, overplot[s].ys, 'k')
        else:
            plt.plot(full.ts, full.ys, 'k')
        treat_axes(s, n_subplots, title='Sampled %.1f Hz' % (sampled[s].framerate,))

    if 'save' in kwargs:
        aux.save_figure(kwargs['save'])

    plt.show()

