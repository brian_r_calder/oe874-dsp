{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Notebook Intent\n",
    "\n",
    "When applying a filter, there are two choices: apply in the time domain, or apply in the frequency domain.  A filter applied in the frequency domain is usually called \"fast convolution\" since it can, under some circumstances, be significantly faster than the alternative.  There are, however, some constraints to be aware of in doing so if you want the effects to be the same as what you'd get from \"linear convolution\" (i.e., application in the time domain).\n",
    "\n",
    "In this notebook, the examples illustrate what the contraints are, and how you can avoid problems.\n",
    "\n",
    "For copyright and license information, see footnote.\n",
    "\n",
    "# Required Resources\n",
    "\n",
    "We need [ThinkDSP](https://greenteapress.com/wp/think-dsp/) for signal representation and sampling, SciPy for the DSP module (`signal`), and then a support file (`fast_convolution_plots`) to do plotting of outputs:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import thinkdsp as dsp\n",
    "from scipy import signal\n",
    "import fast_convolution_plots as disp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Initial Signal Setup\n",
    "\n",
    "To start with, we need a signal to filter; we generate one at 440Hz (audible range and nominally Concert Pitch), sampled at 8kHz, and then sample independent, identically distributed Gaussian noise (`dsp.UncorrelatedGaussianNoise()`) to make a noisy version of the signal to filter:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "samp_freq = 8000\n",
    "signal_duration = 1.0\n",
    "signal_freq = 440\n",
    "noise_amplitude = 0.5\n",
    "\n",
    "signal_model = dsp.SinSignal(freq=signal_freq, amp=1.0, offset=0.0)\n",
    "noise_model = dsp.UncorrelatedGaussianNoise(amp=noise_amplitude)\n",
    "\n",
    "pure = signal_model.make_wave(duration=signal_duration, framerate=samp_freq)\n",
    "noise = pure + noise_model.make_wave(duration=signal_duration, framerate=samp_freq)\n",
    "\n",
    "disp.display_start_point(pure, noise)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The comparison of the clean (blue) and noisy (orange) signals clearly demonstrate the effects of the noise added.  This isn't a huge amount of noise in the grant scheme of things, as you can see from the periodogram estimate of the Power Spectral Density: the noise is -80dB relative to the signal over the 1.0s duration of the signal.  However, it will illustrate the point.  You can also hear the difference:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pure.make_audio()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "noise.make_audio()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Band-pass Filter Construction & Application\n",
    "\n",
    "Since we know the target frequency for the signal, we can readily construct a filter to reduce the noise everywhere except where the signal is.  To give us decent suppression, we choose a relatively long filter length:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "filter_length = 101\n",
    "corner_freq = [0.9*signal_freq, 1.1*signal_freq]\n",
    "bp_imp = signal.firwin(filter_length, corner_freq, fs=samp_freq, pass_zero=False)\n",
    "bp_filter = dsp.Wave(bp_imp, framerate=samp_freq)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The \"normal\" means of applying the filter would be to use the `convolve()` method, also known as \"linear convolution\", applied in the time domain, giving output $y(n) = x(n)*h(n)$ for input $x(n)$ and filter impulse response (i.e., filter coefficients in this case), $h(n)$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "linear_conv = noise.convolve(bp_filter)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As an alternative, we can also use the identity\n",
    "\n",
    "$y(n) = x(n)*h(n) \\leftrightarrow Y(\\omega) = X(\\omega)H(\\omega)$\n",
    "\n",
    "to recognise that if we transform the signal, and the filter impulse response, into the frequency domain ($X(\\omega)$ and $H(\\omega)$), then a simple multiplication will generate the output $Y(\\omega)$.  We can then inverse transform to the time domain again to give us an estimate of $y(n)$.  This is known as \"circular convolution\" for reasons we'll get in to later.\n",
    "\n",
    "There's a small technical hitch to this in that the signals have to be the same length in order to do this, so we need to pad out the filter coefficients to the length of the signal before processing:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bp_padded = bp_filter.copy()\n",
    "bp_padded.zero_pad(len(noise))\n",
    "bp_spec = bp_padded.make_spectrum()\n",
    "\n",
    "noise_spec = noise.make_spectrum()\n",
    "\n",
    "circular_spec = bp_spec * noise_spec\n",
    "circular_conv = circular_spec.make_wave()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we generate plots of the two different versions, however, we can immediately see that there's a significant problem in that the results are not the same:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "disp.display_padded(bp_filter, noise, linear_conv, circular_conv)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This difference is due to the fact that we're using the Discrete Fourier Transform (DFT) for processing (i.e., the digital domain version of the Fourier Transform), which doesn't generate a continuous spectrum across frequency, but samples in the frequency domain so that there are only a finite number of harmonically related frequencies that can be represented.  If we use a long enough signal, we can make this an arbitrary number, but it's still finite.\n",
    "\n",
    "Like sampling in the time domain leads to circularity in the frequency domain (this is what causes aliasing effects), sampling in the frequency domain causes circularity in the time domain.  Consequently, even if it isn't true for your particular signal, using the DFT means that an assumption that your signal (or filter) is one period of a period signal is baked in to the analysis.  You get this whether you like it or not.\n",
    "\n",
    "The practical upshot of this is the effect above: because the signal and (padded) filter are the same length, if you think of them repeated in time (i.e., with circularity assumption from the DFT), the end of the signal from one repeat overlaps, partially, with the filter for the previous repeat, and vice versa.  In effect, you're not actually filtering the same signal.\n",
    "\n",
    "# Padded Linear-Equivalent Circular Convolution\n",
    "\n",
    "We can get around this, however, by simply padding a little more.  We need to ensure that we add enough zeros to the end of both signals to make sure that there's no overlap between different repeats of both of them when they're lined up together for convolution.  In effect, this makes a circular convolution (via the frequency domain) that's equivalent to linear convolution (via the time domain).\n",
    "\n",
    "If you think through the consequences, you'll recognise that this means that we need both signals to be length $N + M - 1$ if we have signals of length $N$ and $M$:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bp_padded = bp_filter.copy()\n",
    "bp_padded.zero_pad(len(bp_filter) + len(noise) + 1)\n",
    "bp_spec = bp_padded.make_spectrum()\n",
    "\n",
    "noise_padded = noise.copy()\n",
    "noise_padded.zero_pad(len(bp_filter) + len(noise) + 1)\n",
    "noise_spec = noise_padded.make_spectrum()\n",
    "\n",
    "circular_spec = bp_spec * noise_spec\n",
    "circular_conv = circular_spec.make_wave()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, the linear and circular convolution generate the same behavior:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "disp.display_padded(bp_filter, noise, linear_conv, circular_conv)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Other Things to Try\n",
    "\n",
    "- Although it's nominally called \"fast\" convolution, it isn't always faster to transform into the frequency domain because the transformation costs some time too.  Try some different lengths of signal and filter, and see if you can determine what the limits are.\n",
    "- The naive method of processing (i.e., transforming both signal and filter each time) is actually more expensive than is necessarily required: if your filter isn't changing, you can transform it once and then cache the result, amortizing the cost over multiple filtering passes for differing input records.  Generate an implementation of this idea, and try the timings again.\n",
    "\n",
    "# Copyright and License\n",
    "\n",
    "Copyright 2019, University of New Hampshire, Center for Coastal and Ocean Mapping.\n",
    "\n",
    "This program is free software: you can redistribute it and/or modify\n",
    "it under the terms of the GNU General Public License as published by\n",
    "the Free Software Foundation, either version 3 of the License, or\n",
    "(at your option) any later version.\n",
    "\n",
    "This program is distributed in the hope that it will be useful,\n",
    "but WITHOUT ANY WARRANTY; without even the implied warranty of\n",
    "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n",
    "GNU General Public License for more details.\n",
    "\n",
    "You should have received [a copy of the GNU General Public License](COPYING)\n",
    "along with this program.  If not, see [here](https://www.gnu.org/licenses/)."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
