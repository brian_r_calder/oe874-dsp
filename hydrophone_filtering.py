#   Copyright 2019, University of New Hampshire, Center for Coastal and Ocean Mapping.
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

import thinkdsp as dsp
import dsp_aux as aux
import hydrophone_filtering_plots as disp
from scipy import signal

# Make sure that the auxdata directory exists for output wave files
aux.make_auxdata_dir()

record = dsp.read_wave('hydrophone.wav')
disp.show_spectrogram(record, save='original_hydrophone_spectrogram')

filter_length = 201

# Generate a high-pass filter with corner frequency at 2.5kHz to reduce the ship
# noise without removing too much of the sub-bottom profiler
corner_freq = 2500.0
hp_imp = signal.firwin(filter_length, corner_freq, window='blackmanharris', fs=record.framerate, pass_zero=False)
sig_hp = record.convolve(hp_imp)
disp.show_spectrogram(sig_hp, save='highpass_hydrophone_spectrogram')
sig_hp.write('auxdata/hp_hydrophone.wav')

# Generate a band-pass filter around the multibeam data (9.5 - 15.5 kHz, roughly)
corner_freq = [9500, 15500]
bp_imp = signal.firwin(filter_length, corner_freq, window='blackmanharris', fs=record.framerate,  pass_zero=False)
sig_mbes = record.convolve(bp_imp)
disp.show_spectrogram(sig_mbes, save='mbes_hydrophone_spectrogram')
sig_mbes.write('auxdata/mbes_hydrophone.wav')

# Generate a band-pass filter around the sub-bottom data (2.5 - 6.5 kHz, roughly)
corner_freq = [2500, 6500]
bp_imp = signal.firwin(filter_length, corner_freq, window='blackmanharris', fs=record.framerate,  pass_zero=False)
sig_sbp = record.convolve(bp_imp)
disp.show_spectrogram(sig_sbp, save='sbp_hydrophone_spectrogram')
sig_sbp.write('auxdata/sbp_hydrophone.wav')
