#   Copyright 2019, University of New Hampshire, Center for Coastal and Ocean Mapping.
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

from scipy import signal
import numpy as np
import matplotlib.pyplot as plt
import dsp_aux as aux


def display_fir_filters(filters, sampling_frequency, **kwargs):
    def treat_axes(vert_label, max_time, min_amp, max_amp):
        plt.xlabel('Time (s)')
        plt.ylabel(vert_label)
        plt.grid()
        plt.axis((-max_time, max_time, min_amp, max_amp))

    period = 1.0 / sampling_frequency
    n_subplots = len(filters)

    # Optional keyword arguments for line style and ylabel
    if 'linestyle' in kwargs:
        linespec = kwargs['linestyle']
    else:
        linespec = 'ko-'
    if 'ylabel' in kwargs:
        vert_label = kwargs['ylabel']
    else:
        vert_label = 'Filter Coefficient'

    # Go through the filters, and find the maximum/minimum amplitude, and time
    max_time = 0.0
    min_amp = 0.0
    max_amp = 0.0
    for f in range(n_subplots):
        t = period * len(filters[f]) / 2
        if t > max_time:
            max_time = t

        min_a = filters[f].min()
        if min_a < min_amp:
            min_amp = min_a

        max_a = filters[f].max()
        if max_a > max_amp:
            max_amp = max_a

    # Generate a stack of N plots, all on the same axes for comparison
    plt.figure(figsize=(14, 10))
    for f in range(n_subplots):
        plt.subplot(n_subplots, 1, f + 1)
        ts = np.linspace(0, period * len(filters[f]), len(filters[f])) - period * len(filters[f]) / 2
        plt.plot(ts, filters[f], linespec)
        if 'legend' in kwargs:
            legend_text = kwargs['legend'][f]
        else:
            legend_text = 'N = %d' % (len(filters[f]),)
        plt.legend((legend_text,))
        treat_axes(vert_label, 1.1 * max_time, 1.1 * min_amp, 1.1 * max_amp)

    if 'save' in kwargs:
        aux.save_figure(kwargs['save'])


def display_fir_spectra(filters, sampling_frequency, **kwargs):
    def treat_axes(ax, sampling_frequency, filter_length, plotnum, **kwargs):
        ax.set_xlabel('Frequency (Hz)')
        if 'ylabel' in kwargs:
            ax.set_ylabel(kwargs['ylabel'])
        else:
            ax.set_ylabel('Filter Amplitude (dB)')
        if 'legend' in kwargs:
            ax.legend((kwargs['legend'][plotnum],))
        else:
            label = 'N = %d' % (filter_length,)
            ax.legend((label,))

        ax.grid()
        ax.set(xlim=(0, sampling_frequency / 2.0))
        if 'linrange' in kwargs:
            ax.set(ylim=kwargs['linrange'])
        else:
            if 'logrange' in kwargs:
                ax.set(ylim=kwargs['logrange'])
            else:
                ax.set(ylim=(-40, 5))

    n_subplots = len(filters)
    plt.figure(figsize=(14, 10))
    for f in range(n_subplots):
        fs, hs = signal.freqz(filters[f], fs=sampling_frequency)
        plt.subplot(n_subplots, 1, f + 1)
        plt.plot(fs, abs(hs), 'k')
        treat_axes(plt.gca(), sampling_frequency, len(filters[f]), f, ylabel='Filter Amplitude', linrange=(-0.1, 1.2),
                   **kwargs)

    if 'save' in kwargs:
        aux.save_figure(kwargs['save'])

    plt.figure(figsize=(14, 10))
    for f in range(n_subplots):
        fs, hs = signal.freqz(filters[f], fs=sampling_frequency)
        plt.subplot(n_subplots, 1, f + 1)
        plt.plot(fs, 20.0 * np.log10(abs(hs)), 'k')
        treat_axes(plt.gca(), sampling_frequency, len(filters[f]), f, **kwargs)

    if 'save' in kwargs:
        aux.save_figure(kwargs['save'])
