#   Copyright 2019, University of New Hampshire, Center for Coastal and Ocean Mapping.
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

import dsp_aux as aux
import matplotlib.pyplot as plt
import numpy as np


def plot_time_and_spectrum(signal, title, **kwargs):
    spectrum = signal.make_spectrum()

    plt.figure(figsize=(14, 10))

    plt.subplot(2, 1, 1)
    plt.plot(signal.ts, signal.ys, '.-')
    plt.xlabel('Time (s)')
    plt.ylabel('Amplitude')
    plt.title(title)
    plt.grid()

    plt.subplot(2, 1, 2)
    plt.stem(spectrum.fs, spectrum.amps / len(spectrum.amps), use_line_collection=True)
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Magnitude')
    plt.xlim(0, 20)
    plt.xticks(np.arange(0, 20, step=1.0))
    plt.grid()

    if 'save' in kwargs:
        aux.save_figure(kwargs['save'])

    plt.show()
