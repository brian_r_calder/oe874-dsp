#   Copyright 2019, University of New Hampshire, Center for Coastal and Ocean Mapping.
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

import thinkdsp as dsp
import dsp_aux as aux
import play_note_sequence_plots as disp

# Make sure that the auxdata directory is available for outputs
aux.make_auxdata_dir()

signal = dsp.Chirp(start=100, end=7999, amp=1.0)

wave_hifi = signal.make_wave(duration=10.0, framerate=16000)
wave_hifi.write('auxdata/linear_chirp_16kHz.wav')
disp.display_spectrum(wave_hifi, save='linear_chirp_spectrogram')

wave_lofi = signal.make_wave(duration=10.0, framerate=8000)
wave_lofi.write('auxdata/linear_chirp_8kHz.wav')
disp.display_spectrum(wave_lofi, save='linear_chirp_aliased_spectrogram')
