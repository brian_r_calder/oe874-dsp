{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Notebook Intent\n",
    "\n",
    "Most of the signals that we want to process start in the real world as continuously varying signals (\"continuous time signal\"), but if we want to process them with digital methods, we need to convert them into a series of numbers in the computer.  How we do that is important, and can have a significant effect on how well the digital filtering matches what we might see if we did the filtering in continuous time (which is also possible, but not the subject of this module).  This notebook is intended to help you understand how we do the conversion to digital form, and the consequencies for not doing it correctly.\n",
    "\n",
    "For copyright and license information, see footnote.\n",
    "\n",
    "# Required Resources\n",
    "\n",
    "We need [ThinkDSP](https://greenteapress.com/wp/think-dsp/) for signal representation and sampling, SciPy for interpolation methods (`interpolate`) used for reconstruction, and support code (`sampling_reconstruction_plots`) to display results:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import thinkdsp as dsp\n",
    "import sampling_reconstruction_plots as disp\n",
    "from scipy import interpolate"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Basic Signal\n",
    "\n",
    "Since we're working in the digital domain all the time, we can't really have a truly continuous time signal; however, we can approximate it by taking the samples of the signal very quickly.  In this case, we'll make that approximation by sampling at 16kHz:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "full_sampling_frequency = 16000"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To keep things simple, consider a sinusoid with frequency 5Hz (`freq=5.0`), and unit amplitude (`amp=1.0`), which we sample at the \"almost continuous\" frequency (`framerate=full_sampling_freqency`):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "base = dsp.CosSignal(freq=5.0, amp=1.0, offset=0.0)\n",
    "full = base.make_wave(duration=1.0, framerate=full_sampling_frequency)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Effects of Sampling at Different Rates\n",
    "\n",
    "Consider first sampling the 5Hz signal at three different rates: 32Hz, 16Hz, and 8Hz.  We can change the sampling rate by setting the `framerate` parameter, and catch the results in a list for display later:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "samp_rates = [32, 16, 8]\n",
    "sigs = []\n",
    "for f in samp_rates:\n",
    "    w = base.make_wave(duration=1.0, framerate=f)\n",
    "    sigs.append(w)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "On the face of things, when we show the original signal in addition to the samples, things look reasonable in the sense that the samples match up with the original in each case:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "disp.display_signals(full, sigs)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Remember, however, that the signal that we have is only the samples, and not anything in between: there's only the sequence of numbers to work with.  If we want to do comparisons with the original signal (or at least our best approximation of it, as above), and therefore understand the effects of the different sampling rates, then we need to make an appropriate reconstruction of the signal.\n",
    "\n",
    "# Signal Reconstructions\n",
    "\n",
    "We can choose any number of different methods to take the series of numbers that are the basic form of the sampled (digital) signal, and reconstruct an approximation to the original signal.  The method and complexity of the reconstruction method has significant effect on the quality of the approximation, and highlight the differences in sampling rate.\n",
    "\n",
    "In the example here, we reconstruct using three different methods:\n",
    "- Zero order hold (also known as `previous`, since the reconstruction is based on the previous sample in time).\n",
    "- First order hold (also known as `linear`, since the reconstruction joins the dots between the samples with a straight line).\n",
    "- Sinc interpolation (`sinc`) which uses an optimal reconstruction based on the sinc function,\n",
    "\n",
    "$$x(n) = \\frac{\\sin(\\pi n)}{\\pi n}$$\n",
    "\n",
    "The `previous` and `linear` methods are available through SciPy's `interpolate` module; the `sinc` interpolation is implemented in the support code (which is a little slower, since it's pure Python).  Again, we do all of this at the same time, and cache the results for display later; we apply each interpolation method to each sampling rate:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "interps = ['previous', 'linear', 'sinc']\n",
    "methods = []\n",
    "for interp in interps:\n",
    "    reconstructions = []\n",
    "    for s in range(len(samp_rates)):\n",
    "        extended_sig = base.make_wave(duration=5.0, framerate=samp_rates[s])\n",
    "        if interp == 'sinc':\n",
    "            w = disp.sinc_interpolate(extended_sig.segment(start=0, duration=1.0), full.framerate)\n",
    "        else:\n",
    "            interpolator = interpolate.interp1d(extended_sig.ts, extended_sig.ys, kind=interp)\n",
    "            w = dsp.Wave(interpolator(full.ts), ts=full.ts, framerate=full.framerate)\n",
    "        reconstructions.append(w)\n",
    "    methods.append(reconstructions)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Zero-order Reconstruction\n",
    "\n",
    "If we use zero-order hold, each signal is reconstructed with the current sample until the next one occurs:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "disp.display_signals(full, sigs, overplot=methods[0])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It's hard to tell what signal you have with this method due to the crudity of the reconstruction - there isn't a lot of similarity between the reconstruction and the original.  You can see, however, that there is some oscillation about the zero value, and at least for 32Hz and 16Hz sampling the period of the oscillation is about the same as the original signal.  The data samppled at 8Hz, however, does not appear to have the same oscillation rate, which suggests that it isn't the same signal (since it's not at the same frequency).\n",
    "\n",
    "# First-order Reconstruction\n",
    "\n",
    "If we improve the reconstruction method to first-order, the results are more illustrative and informative:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "disp.display_signals(full, sigs, overplot=methods[1])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The equivalence of the 32Hz sampled signal to the original is now much more clear, and the 16Hz version is a cruder, but reasonable approximation: it's pretty clear that they're the same signal.  It's also clear, however, that the 8Hz version is definitely not the same: even if you approximate the period of the signal by counting the zero crossings of the reconstruction, and then convert that to a frequency, it's decidedly different from the original 5Hz signal frequency.  Clearly, something odd is going on.\n",
    "\n",
    "# Optimal Reconstruction\n",
    "\n",
    "Using the optimal `sinc` reconstruction, the results are much more clear:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "disp.display_signals(full, sigs, overplot=methods[2])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here, the signal at 32Hz and 16Hz are recognizably the same as the original (there's a slight difference because we can only approximate true sinc-interpolation digitally), but the 8Hz reconstruction is very different.  Here, the period is clearly order 0.35s, which would make the signal 3Hz rather than 5Hz!\n",
    "\n",
    "Clearly, therefore, the rate of sampling makes a significant difference to the signal that we have in digital form, and if don't get it right, the digital version of the signal is not going to be the same as the original continuous time version, and therefore any results that we generate through digital filtering are not going to match what we'd get from working on the original signal.  Which is not what we want.\n",
    "\n",
    "In order to be effective, therefore, we need to better understand the effects of sampling, and the relationship between the sampling frequency and the signal frequency, and the conditions required for equivalence to the original signal.\n",
    "\n",
    "# Other Things To Try\n",
    "\n",
    "- Stay with low signal frequencies, but vary the signal frequency and the sampling frequency to see how they interact.  You'll get better (more interesting!) results if you use signal frequencies that are close to zero, or close to the sampling frequency.\n",
    "- You could approach this problem either by holding the sampling frequency stable and changing the signal frequency, or holding the signal frequency stable and changing the sampling frequency.  Do both, and investigate the results.\n",
    "- After you do some preliminary investigation, try signal frequencies slightly above, and slightly below half the sampling frequency, and see if you can predict the results before you look at them.\n",
    "\n",
    "# Copyright and License\n",
    "\n",
    "Copyright 2019, University of New Hampshire, Center for Coastal and Ocean Mapping.\n",
    "\n",
    "This program is free software: you can redistribute it and/or modify\n",
    "it under the terms of the GNU General Public License as published by\n",
    "the Free Software Foundation, either version 3 of the License, or\n",
    "(at your option) any later version.\n",
    "\n",
    "This program is distributed in the hope that it will be useful,\n",
    "but WITHOUT ANY WARRANTY; without even the implied warranty of\n",
    "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n",
    "GNU General Public License for more details.\n",
    "\n",
    "You should have received [a copy of the GNU General Public License](COPYING)\n",
    "along with this program.  If not, see [here](https://www.gnu.org/licenses/)."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
