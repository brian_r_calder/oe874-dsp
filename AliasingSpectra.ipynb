{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Notebook Intent\n",
    "\n",
    "Having examined aliasing through straight sampling (`SamplingReconstruction` notebook) and with an audio example (`PlayNoteSequence`), it's clear that the sampling rate and frequency content of the signals being sampled are related.  This notebook attempts to highlight the effect, and thereby motivate an intuitive understanding of the [Nyquist-Shannon Sampling Theorem](https://en.wikipedia.org/wiki/Nyquist%E2%80%93Shannon_sampling_theorem).\n",
    "\n",
    "For copyright and license information, see footnote.\n",
    "\n",
    "# Required Resources\n",
    "\n",
    "In this case, we require only the suppoting code (`aliasing_spectra_plots`) to generate plots; that code loads all of the other modules that are required:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import aliasing_spectra_plots as disp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Sampled Signal Spectra\n",
    "\n",
    "Consider two different versions of the same signals, sampled at different rates.  We'll fix the sampling frequencies at 2kHz and 8kHz:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lofi_sampling_frequency = 2000\n",
    "hifi_sampling_frequency = 8000"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To start, consider a 400Hz input signal sampled at the two different rates, and then converted into the frequency domain, so that we can see where the power in the signals is concentrated:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lo, hi = disp.make_spectra(400, lofi_sampling_frequency, hifi_sampling_frequency)\n",
    "disp.spectrum_comparison(lo, hi)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The lengths of the spectra are different due to the different sampling frequencies, but you can see that the power is concentrated at 400Hz in both cases as we might expect.\n",
    "\n",
    "If we increase the frequency to 900Hz and do the same thing again:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lo, hi = disp.make_spectra(900, lofi_sampling_frequency, hifi_sampling_frequency)\n",
    "disp.spectrum_comparison(lo, hi)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "it's clear that we still have equivalence between the two different sampling frequencies, with power correctly indicated at 900Hz in both cases.\n",
    "\n",
    "If, however, we increase the signal frequency to 1100Hz, things start to deivate between the two cases:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lo, hi = disp.make_spectra(1100, lofi_sampling_frequency, hifi_sampling_frequency)\n",
    "disp.spectrum_comparison(lo, hi)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here, it's clear that the signal sampled at 8kHz shows the signal at 1100Hz as expected, but the one sampled at 2kHz shows the result at 900Hz instead.\n",
    "\n",
    "We can gather a little more insight by increasing the fequency again, this time to 1600Hz:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lo, hi = disp.make_spectra(1600, lofi_sampling_frequency, hifi_sampling_frequency)\n",
    "disp.spectrum_comparison(lo, hi)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It's now clear that the 2kHz version shows up at 400Hz (compare the first result) rather than 1600Hz as it does with 8kHz sampling.  A little careful thought makes it clear that the relationship is that the frequencies are mirrored at half the sampling frequency: 900Hz is 100Hz below 2000Hz / 2 rather than 100Hz above; 400Hz is 600Hz below 2000 Hz /2 = 1000 Hz, rather than 600Hz above.\n",
    "\n",
    "# Nyquist-Shannon Sampling Theorem\n",
    "\n",
    "This relationship is not conincidental, and is a direct consequence of sampling the signals at all, and specifically at the given rate.  One way to think about this is to imagine that instead of having a linear frequency scale, you bend the ends of the scale around into a circle so that, for the 2kHz example, the piece at 1000Hz is butted up against -1000Hz.  If you then consider the 1100Hz example, you can interpret the result as moving out along the circular frequency spectrum 1100Hz from zero, bearing in mind that you will loop around at 1000Hz, and end up at -900Hz; the other part of the spectrum, which would normally be at -1100Hz will also loop around at -1000Hz, and end up at +900Hz instead.  If you make the plot, it's not evident that the different components have looped around, so what you see it the plot with spikes at 900Hz:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lo, hi = disp.make_spectra(1100, lofi_sampling_frequency, hifi_sampling_frequency)\n",
    "disp.spectrum_comparison(lo, hi)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The same, of course, can be done with the 1600Hz signal: the +1600Hz version loops round at 1000Hz and ends up at -400Hz, while the one at -1600Hz loops round at -1000Hz and turns up at +400Hz.\n",
    "\n",
    "This concept is know as \"circular frequency\", and is fundamental to digital signal processing.  What it tells us is that after a signal reaches half the sampling frequency, it's impossible to distinguish it from another signal at a frequency that's reflected in the half-way point around the circle.  Therefore, if we want to have a unique representation of an input signal, we either have to make sure that the input signal doesn't have any frequencies aove half the sampling rate we set, or that we sample at a sufficiently high rate to make sure that we accommodate the input signal's highest frequency.\n",
    "\n",
    "This \"critical frequency\" is important enough to have its own name, and is usually called the Nyquist Frequency.  The requirement that the signal frequency be at most half the sampling frequency for correct representation in digital form (also know as \"perfect reconstruction\") is called the Nyquist-Shannon Sampling Theorem.\n",
    "\n",
    "Other consequences:\n",
    "- If, instead of single frequencies, you have a range of frequencies in the input signal, the effect of this theorem is that you end up with a copy of the continuous signal's spectrum at all multiples of the sampling frequency.  These \"aliases\" of the \"baseband\" spectrum are what give the phenomenon its name.\n",
    "- You always get this effect when you sample a signal: it's a direct result of the sampling.  It's therefore impossible to avoid.\n",
    "- If you keep in mind the picture of copies of the baseband spectrum at the multiples of the sampling frequency, and slowly reduce the sampling frequency, you'll eventually have the spectra start of overlap with their neighbors.  This is equivalent to the onset of damaging aliasing (i.e., when the highest frequency is more than the Nyquist frequency).\n",
    "- You don't have to stop at one cycle round the circular frequency axis: after you get back to zero (which is also equivalent to the sampling frequency), you can keep going for more loops.  Since you're always moving in one direction, the frequency is always increasing (or decreasing, depending on your cycle direction), and therefore you're always going more and more positive (or negative), but keep encountering the same frequencies with significant power on each loop.  This is another way of forming the aliases spectra model above.\n",
    "- Although the minimum sampling frequency is twice the highest input signal frequency, in practice it's generally insufficient, and something closer to four or five times the Nyquist frequency is generally preferred.\n",
    "- We usually motivate this model by saying that you need to sample at twice the highest frequency, but that's a simplified version of the truth (which is that you need to sample the input at a rate at least twice the bandwidth of the signal, for a bandwidth limited signal).  In practice, this means that some systems can sample at significantly lower rates than might be expected, and in fact are sampling one of the aliases, rather than the actual signal!  This has particular application in sonar systems, where you have a generally low-bandwidth frequency range of interest around a significantly higher frequency (e.g., a 10kHz bandwidth signal around a base frequency of 100kHz).  Practically, this lets you have slower (and therefore cheaper and/or higher accuracy) sampling circuitry.\n",
    "\n",
    "# Other Things to Try:\n",
    "- Convince yourself that you understand the nature of circular frequency by experimenting with different signal frequencies and sampling frequencies.  In particular, try a signal frequency more than the sampling frequency (i.e., not just between the Nyquist frequency and the sampling frequency, as here), or three times the sampling frequency, and try to predict where it'll appear once sampled.\n",
    "- Try bandwidth sampling: set up a signal with components at 95kHz, 100kHz, and 105kHz, making the amplitude of each different (you can do this by making three `dsp.SinusoidSignal()` objects, and then adding them together).  Sample at 250kHz (make a small duration, or it'll be massive), and verify the signal; then sample at 20kHz and see what you have afterwards.  Change up the sampling frequencies, and see what you get in the frequency domain.\n",
    "\n",
    "# Copyright and License\n",
    "\n",
    "Copyright 2019, University of New Hampshire, Center for Coastal and Ocean Mapping.\n",
    "\n",
    "This program is free software: you can redistribute it and/or modify\n",
    "it under the terms of the GNU General Public License as published by\n",
    "the Free Software Foundation, either version 3 of the License, or\n",
    "(at your option) any later version.\n",
    "\n",
    "This program is distributed in the hope that it will be useful,\n",
    "but WITHOUT ANY WARRANTY; without even the implied warranty of\n",
    "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n",
    "GNU General Public License for more details.\n",
    "\n",
    "You should have received [a copy of the GNU General Public License](COPYING)\n",
    "along with this program.  If not, see [here](https://www.gnu.org/licenses/)."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
