{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Notebook Intent\n",
    "\n",
    "It's very important to understand the relationship between the sampling frequency used in making a digital representation of a signal, and the frequency content of the signal; if you get this wrong, all sorts of Bad Things can happen.  In particular, your digital representation will not be equivalent to the original signal, so any results you generate will be unrealistic.  This notebook illustrates what can go wrong through an audio example.\n",
    "\n",
    "For copyright and license information, see footnote.\n",
    "\n",
    "# Required Resources\n",
    "\n",
    "We need [ThinkDSP](https://greenteapress.com/wp/think-dsp/) for signal representation and audio file construction, and support code (`play_note_sequence_plots`) for plot display:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import thinkdsp as dsp\n",
    "import play_note_sequence_plots as disp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Basic Signal\n",
    "\n",
    "Since we know (from the `SamplingReconstruction` notebook) that the frequency of the sampling with respect to the signal frequency is an important feature, we can investigate the behavior over a range of frequencies by making a [chirp signal](https://en.wikipedia.org/wiki/Chirp) that starts at 100Hz (`start=100`) and changes frequency linearly until it ends at 7999Hz (`end=7999`) some time later, with unit amplitude (`amp=1.0`):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "signal = dsp.Chirp(start=100, end=7999, amp=1.0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Effects of Sampling Rate\n",
    "\n",
    "As before, we can't have a true continuous time signal, but we can approximate it by setting the sampling frequency sufficiently high.  In this case 'sufficient' is 16kHz (`framerate=16000`), and we generate a signal lasting 10s (`duration=10`):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "wave_hifi = signal.make_wave(duration=10.0, framerate=16000)\n",
    "wave_hifi.make_audio()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Listen to the audio (be careful about volume: it might be quite loud if you're using headphones, or depending on your system).  You should hear a signal that starts at low frequency, and steadily ramps up to a high pitch (although it should still be audible, so long as your hearing is intact!) over the course of 10s.\n",
    "\n",
    "We can make a visual version of this by computing what's called the \"spectrogram\".  This representation shows the frequency content of the signal (just like a spectrum) computed at a particular time on the vertical axis, with different computation times on the horizontal axis; the color-coding corresponds to magnitude of the various frequency components (as shown in the colorbar to the right), displayed here in logarithmic (decibel) units.  In this way, you can see the evolution of the signal's frequency content as a function of time:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "disp.display_spectrum(wave_hifi)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This display clearly demonstrates the linear nature of the frequency change in the signal, from 100Hz up to nearly 8kHz.  The colors off-axis are due to the finite nature of the approximations that we're making, and the computation method; ideally, it would be a perfect ridge.\n",
    "\n",
    "Consider now, however, what happens if we change the sampling rate to half the value, at 8kHz, but otherwise keep things the same:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "wave_lofi = signal.make_wave(duration=10.0, framerate=8000)\n",
    "wave_lofi.make_audio()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Listen to the audio again (before you execute the next cell, or read on!), with the same caveats with respect to volume.  You'll clearly recognize that the result is significantly different.\n",
    "\n",
    "As before, we can see what that looks like by computing the spectrogram:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "disp.display_spectrum(wave_lofi)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Clearly, the frequency change is still linear, it's just that this time it's not always in the same direction: the signal eventually turns around and rolls back to low frequency again.  Significantly, the point at which the signal turns around is exactly 4kHz, or half of the sampling frequency.\n",
    "\n",
    "This frequency (half the sampling rate) is significant enough that it gets its own name: the Nyquist Frequency, named after [Harry Nyquist](https://en.wikipedia.org/wiki/Harry_Nyquist), a Swedish-American electrical engineer who first articulated many of the ideas we're considering here.\n",
    "\n",
    "This effect is called \"aliasing\", because it causes signals to appear at a different frequency than might be expected (i.e., at an \"alias\", or disguised, frequency), and is a direct consequence of sampling the original signal in time.  That is, given that you sample in time, you're going to get this effect at some level: there's no getting away from it, and once it's happened, there's no fixing it.  The best that you can do, in practice, is to avoid it being a significant effect through what are known as \"anti-aliasing\" techniques.\n",
    "\n",
    "The fundamental problem is that the sampling rate is insufficiently high to adequately represent the signal being sampled: as you saw above, if you increase the sampling rate to at least twice the highest frequency in the input signal, the representation is exact.  (This is known as the [Nyquist-Shannon Sampling Theorem](https://en.wikipedia.org/wiki/Nyquist%E2%80%93Shannon_sampling_theorem), named after Nyquist, and [Claude Shannon](https://en.wikipedia.org/wiki/Claude_Shannon), a pioneer of information theory.)  Therefore, there are two basic approaches to anti-aliasing:\n",
    "1. You ensure that the input signal has no frequency content above half the sampling frequency that you're willing to use.  Usually this is done by filtering the signal, very approximately, in continuous time (i.e., in the real world, with some filtering electronics) with a cut-off at the Nyquist frequency, before sampling.\n",
    "2. You sample the input signal at a massively high rate so that the Nyquist frequency is well in excess of any frequency content in the input.  This relies on the physical properties of most signals, which is that they mostly originate in mechanical systems, and therefore have an upper limit of frequency content due to physical constraints on the system.\n",
    "\n",
    "The first option forces you to change your signal to conform to the sampling frequency you can provide, which of course changes what you're processing; it also requires analog electronic circuits, which can be difficult to specify directly, and may change with time as the components age (they're also difficult to adjust if you have many different signals to sample).  It is, however, efficient in that you don't need to sample at a massively high rate, as with the second option.  The second option, however, avoids any analog electronics, and in practice doesn't mean that you need to have a high sample rate for ever.  After you do the initial sampling, you can digital filter the input to control the frequency content, and then down-sample to the appropriate sampling rate that you need for processing.\n",
    "\n",
    "The first option was traditionally more common when digital electronics were expensive, and high sample rates were difficult to achieve.  As prices for complex and high-speed digital electronics have droped, and speeds and computational power have increased, the second option has become significantly more common.\n",
    "\n",
    "# Copyright and License\n",
    "\n",
    "Copyright 2019, University of New Hampshire, Center for Coastal and Ocean Mapping.\n",
    "\n",
    "This program is free software: you can redistribute it and/or modify\n",
    "it under the terms of the GNU General Public License as published by\n",
    "the Free Software Foundation, either version 3 of the License, or\n",
    "(at your option) any later version.\n",
    "\n",
    "This program is distributed in the hope that it will be useful,\n",
    "but WITHOUT ANY WARRANTY; without even the implied warranty of\n",
    "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n",
    "GNU General Public License for more details.\n",
    "\n",
    "You should have received [a copy of the GNU General Public License](COPYING)\n",
    "along with this program.  If not, see [here](https://www.gnu.org/licenses/)."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
