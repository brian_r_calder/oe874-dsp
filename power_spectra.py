#   Copyright 2019, University of New Hampshire, Center for Coastal and Ocean Mapping.
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

import thinkdsp as dsp
from spectrum import parma, pyule, aryule, arma2psd
from scipy import signal
import numpy as np
import power_spectra_plots as disp

# Default behaviours for all of the signals being generated
samp_freq = 8000        # Hertz
sig_freq = 400          # Hertz

# Generate a noisy signal example: cosine in uncorrelated (IID) Gaussian noise.  Note that the
# level of noise can be quite significant due to the length of the signal that we make
signal_amp = 1.0        # Arbitrary units
sample_duration = 5.0   # seconds
noise_sd = 3.0          # Arbitrary units

orig_signal = dsp.CosSignal(freq=sig_freq, amp=signal_amp, offset=0.0)
noise = dsp.UncorrelatedGaussianNoise(amp=noise_sd)
noisy_signal = orig_signal + noise
orig_wave = orig_signal.make_wave(duration=sample_duration, framerate=samp_freq)
noisy_wave = noisy_signal.make_wave(duration=sample_duration, framerate=samp_freq)

# Generate a display of the input and noisy signal for comparison of the scale of the problem
disp.display_signal([noisy_wave, orig_wave], ['Input Signal', 'Original Signal'],
                    trange=(0, 0.05), srange=(-20, 20), save='psd_test_signal', show=False)

#
# Demonstrate ergodic and non-ergodic signals as motivation for why short-sequence PSD is important
#
unit_noise_signal = dsp.UncorrelatedGaussianNoise(amp=1.0)

# Demonstrate multiple realisations of the same ergodic sample
signals = []
names = []
for s in range(3):
    ergodic_signal = unit_noise_signal.make_wave(duration=3.0, framerate=samp_freq)
    signals.append(ergodic_signal)
    names.append('Realisation %d' % (s+1,))

disp.display_signal(signals, names, trange=(0, 0.05), srange=(-3.5, 3.5), save='psd_noise_realisations', show=False)

# Demonstrate what a non-ergodic signal looks like by comparison
ergodic_signal = unit_noise_signal.make_wave(duration=3.0, framerate=samp_freq)
sd_signal = dsp.CosSignal(freq=1.0, amp=0.5, offset=0.0)
sd_samples = sd_signal.make_wave(duration=3.0, framerate=samp_freq)
sd_samples.bias(1.5)
non_ergodic_signal = ergodic_signal * sd_samples

disp.display_signal([ergodic_signal, non_ergodic_signal], ['Ergodic Noise', 'Non-ergodic Noise'],
                    save='psd_ergodicity', show=False)

#
# Estimate periodogram, and Yule-Walker ARMA model as comparison between non-parametric and parametric solutions,
# respectively.
#
per_f, per_Pxx = signal.periodogram(noisy_wave.ys, fs=samp_freq, nfft=4096)
per = (per_f, per_Pxx)
ar = parma(noisy_wave.ys, 10, 10, 30, NFFT=4096, sampling=samp_freq)

# Display the whole spectrum, and a zoom around the known frequency
disp.display_power_spectra([per, ar], ['Periodogram', 'ARMA(10) Parametric'], mrange=(-75, 25),
                           save='psd_periodogram_arma', show=False)
disp.display_power_spectra([per, ar], ['Periodogram', 'ARMA(10) Parametric'],
                           frange=(sig_freq-100, sig_freq+100), mrange=(-75, 25),
                           save='psd_periodogram_arma_zoom', show=False)

#
# Demonstrate the correlation coefficient for pure noise, and noise with periodicity
#
sd_samples = noise.make_wave(duration=sample_duration, framerate=samp_freq)
noise_acf = signal.correlate(sd_samples.ys, sd_samples.ys) / len(sd_samples.ys)
signal_acf = signal.correlate(noisy_wave.ys, noisy_wave.ys) / len(noisy_wave.ys)

disp.display_acfs([noise_acf, signal_acf], ['Pure Noise', 'Signal + Noise'], samp_freq,
                  arange=(-2.0, 2.0), trange=(-0.01, 0.01), save='psd_autocorrelation', show=False)
disp.display_power_spectra([per], ['Periodogram'], save='psd_periodogram', mrange=(-75, 0), show=False)

#
# Demonstrate the use and properties of the Welch Averaged Periodogram
#

welch_f, welch_Pxx = signal.welch(noisy_wave.ys, fs=samp_freq, nperseg=4096)
welch = (welch_f, welch_Pxx)
disp.display_power_spectra([per, welch], ['Periodogram', 'Welch Periodogram'], mrange=(-75, 0),
                           save='psd_welch_periodogram', show=False)
disp.display_power_spectra([per, welch], ['Periodogram', 'Welch Periodogram'],
                           frange=(sig_freq-100, sig_freq+100), mrange=(-75, 0),
                           save='psd_welch_periodogram_zoom', show=False)

# Add a second signal, close to the first, to look at resolution with different segment lengths, etc.
sig2 = dsp.CosSignal(freq=sig_freq+10, amp=signal_amp, offset=0.0)
orig2_wave = sig2.make_wave(duration=sample_duration, framerate=samp_freq)
noisy_signal = orig_signal + sig2 + noise
noisy_wave = noisy_signal.make_wave(duration=sample_duration, framerate=samp_freq)

analysis_lengths = [512, 1024, 2048, 4096]
spectra = []
for alen in analysis_lengths:
    welch_f, welch_Pxx = signal.welch(noisy_wave.ys, fs=samp_freq, nperseg=alen, nfft=analysis_lengths[-1])
    spectra.append((welch_f, welch_Pxx))

disp.display_sig_and_psd([orig_wave, orig2_wave, noisy_wave], spectra, analysis_lengths,
                         trange=(0, 0.05), frange=(sig_freq-100, sig_freq+100), mrange=(-40, 0),
                         save='psd_welch_averaging', show=False)

#
# Generate noise signal + psd to explain the idea of AR modeling
#

welch_f, welch_Pxx = signal.welch(ergodic_signal.ys, fs=samp_freq, nperseg=4096)
disp.display_sig_and_psd([ergodic_signal], [(welch_f, welch_Pxx)], [4096], trange=(0, 0.05),
                         save='psd_pure_noise_and_psd', show=False)

#
# Generate comparison between periodogram and AR model to illustrate the differences between the two methods
#

system_snr = 20.0                # dB
sig_freqs = [1840.0, 2880.0]    # Hertz

signal_model = None
for s in range(len(sig_freqs)):
    if s == 0:
        signal_model = dsp.CosSignal(freq=sig_freqs[s], amp=1.0, offset=0.0)
    else:
        signal_model += dsp.CosSignal(freq=sig_freqs[s], amp=1.0, offset=0.0)

signal_samples = signal_model.make_wave(duration=2.0, framerate=16000)
noise_stdev = 10**(-system_snr/20.0)/np.sqrt(2.0)
print('Noise standard deviation = %.3f' % (noise_stdev,))
noise_model = dsp.UncorrelatedGaussianNoise(amp=noise_stdev)
noise_samples = noise_model.make_wave(duration=2.0, framerate=16000)
input_samples = noise_samples + signal_samples

disp.display_signal([signal_samples, input_samples], ['Noise Free', 'Input'], trange=(0, 0.05),
                    save='psd_welch_ar_input_data', show=False)

welch_f, welch_Pxx = signal.welch(input_samples.ys[0:10000], fs=16000, nperseg=2048)
input_psd = (welch_f, welch_Pxx)
ar_model = pyule(input_samples.ys[0:10000], 12, sampling=16000, scale_by_freq=False)
ar_psd = (ar_model.frequencies(), ar_model.psd)

disp.display_power_spectra([input_psd, ar_psd], ['Welch Periodogram', 'AR(12) Parametric'],
                           save='psd_welch_ar_psds', show=False)

welch_f, welch_Pxx = signal.welch(input_samples.ys[0:100], fs=16000, nperseg=15, nfft=2048)
short_input_psd = (welch_f, welch_Pxx)
short_ar_model = pyule(input_samples.ys[0:100], 12, sampling=16000, scale_by_freq=False)
short_ar_psd = (short_ar_model.frequencies(), short_ar_model.psd)
disp.display_power_spectra([input_psd, short_input_psd, short_ar_psd],
                           ['Full Signal Welch', 'Short Signal Welch', 'Short Signal AR(12)'],
                           save='psd_welch_ar_short_psds', show=False)

# Finally, look at the behavior of the white noise driving uncertainty as a function of model order
order = np.arange(2, 22, step=2)
psds = []
psd_names = []
residuals = []
for n in order:
    ar, rho, _ = aryule(input_samples.ys[0:100], n, norm='biased')
    psd = arma2psd(ar, rho=rho, T=1.0/16000.0)
    psds.append((np.arange(0, len(psd))*(16000.0/len(psd)), psd))
    psd_names.append('Order = %d' % (n,))
    residuals.append(rho)
disp.display_power_spectra(psds, psd_names, save='psd_yule_order_psds', frange=(0, 8000), show=False)
disp.display_signal([(order, residuals)], ['Residual Uncertainty'], trange=(2, 20),
                    xlabel='AR Model Order', ylabel='Driving White Noise Variance', save='psd_yule_ar_residuals')
