{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Notebook Intent\n",
    "\n",
    "In this example, we're going to work with some data recorded on a hydrophone deployed off the rear deck of the Icebreaker Oden, which is operated by the Swedish Maritime Administration.  At the time, Oden was off northern Greenland during the Ryder 19 Expedition; you can learn more about the expedition from the [Stockholm University website](https://www.su.se/geo/english/research/cruise-logs-reports/expedition-logs/ryder-glacier-expedition-2019-1.445526).  The purpose of the example is to demonstrate the generation of some filters to separate out the signals from different sonars that were running simultaneously during the recording, and to suppress some of the ship noise that was causing difficulties!\n",
    "\n",
    "For copyright and license information, see footnote.\n",
    "\n",
    "# Resources Required\n",
    "\n",
    "We need [ThinkDSP](https://greenteapress.com/wp/think-dsp/) for file access and some functionality, SciPy's signal processing module, and some support code to display plots and do file management."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import thinkdsp as dsp\n",
    "from scipy import signal\n",
    "import hydrophone_filtering_plots as disp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Data Format and Structure\n",
    "\n",
    "To get started, we need to read the WAVE file (`hydrophone.wav`) that contains the data; a sampling rate of 96kHz was used.  In order to get a feel for the data, we can generate a spectrogram, which shows frequency content (vertical axis) as a function of time (horizontal axis).  The code for this is in the supporting file (`hydrophone_filtering_plots.py`) that was imported above.  You can also listen to the data, although you're unlikely to hear anything above about 20kHz, depending on your hearing, and (mostly) your age.  The output from the next cell includes the spectrogram, and an embedded audio clip of five seconds of the recording."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "record = dsp.read_wave('hydrophone.wav')\n",
    "disp.show_spectrogram(record)\n",
    "record.make_audio()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Have a listen to the data, and look at the spectrogram.  You should be able to hear three transmit and receive cycles from both the multibeam echosounder (the energy around 12kHz in the plot), and the sub-bottom profiler (the energy around 5kHz in the plot).  If you have very good hearing, you might just be able to hear the EK80 mid-water echosounder at about 18kHz (but it's unlikely).  You'll probably hear what sounds like more cycles than you might expect; in fact, what you're hearing is the first, and then (fainter) second return from the multibeam transmit.  For example, the transmit cycle at about 0.5s gives the echo at about 1.0s and then at 1.5s or so.\n",
    "\n",
    "You'll also hear, quite strongly, the noise of the ship, including an intermittent clanking noise.  That's the noise of a steam valve used in the fuel heating system that stops the diesel fuel from becoming too viscous to flow through the fuel injectors!  Because it opens and closes quickly, this causes very broadband noise (i.e., extends to significant frequencies more than you might expect), which you can see shortly after the 1.0s mark, and again just before 2.0s, among others.  If this noise coincides with a transmit or receive cycle, then it can cause lots of noise in the data.\n",
    "\n",
    "# Suppressing Ship Noise with an FIR Filter\n",
    "\n",
    "We're going to attempt, first, to isolate the sonar signals from the ship noise by high-pass filtering.  We know that it's mostly low-frequency noise, except for the steam valve, so what we need is a filter length (longer gives more suppression of noise):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "filter_length = 201"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "and then a corner frequency at which to filter.  The corner frequency is determined empirically (i.e., from the data), mostly based on the idea that the sub-bottom profiler signal starts at about 2.5kHz:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "corner_freq = 2500.0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Given the corner frequency, we can then use the Windowed FIR filter design method (`signal.firwin()`) to generate the filter coefficients required to apply a high-pass filter (`pass_zero=False`), and apply it using simple convolution.  We use the [Blackman-Harris window](https://en.wikipedia.org/wiki/Window_function#Blackman%E2%80%93Harris_window) here to give us good side-lobe suppression and reasonable transition from the stop-band (low frequencies in this case) to the pass-band (everything over 2.5kHz):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hp_imp = signal.firwin(filter_length, corner_freq, window='blackmanharris', fs=record.framerate, pass_zero=False)\n",
    "sig_hp = record.convolve(hp_imp)\n",
    "disp.show_spectrogram(sig_hp)\n",
    "sig_hp.make_audio()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Listen to the output audio again, and notice how the ship noise is suppressed, although not totally gone.  It would be difficult to remove it entirely, since there's some overlap between the lowest frequencies of interest for us (the sub-bottom profiler) and the ship's noise.  Getting rid of some of it, however, is still useful.  Notice how the low-frequency component of the spectrogram is all at -140dB or more, reflecting the signal suppression.\n",
    "\n",
    "# Extracting Specific Sonar Signals\n",
    "\n",
    "Suppressing the ship is useful, but if we're interested in getting better performance from the sonars, it's probably more useful to pull out just the frequency range which they use.  For the multibeam echosounder, that range is around 12kHz, but we can see that there's signal from around 9.5-15.5kHz, which leads the selection of corner frequencies for the filter.  In this case, providing a start and stop frequency, and specifying `pass_zero=False` causes the code to generate a band-pass filter with the corner frequencies as given:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "corner_freq = [9500, 15500]\n",
    "bp_imp = signal.firwin(filter_length, corner_freq, window='blackmanharris', fs=record.framerate,  pass_zero=False)\n",
    "sig_mbes = record.convolve(bp_imp)\n",
    "disp.show_spectrogram(sig_mbes)\n",
    "sig_mbes.make_audio()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This filter has clearly suppressed all frequencies outside the range specified, and if you listen to the output, the multibeam transmit-receive cycle (and the second return) is much more clearly audible.  Although there is a small component of the ship's steam-valve noise through the band, it's much lower power than the rest of the signal, so its effect is strongly reduced.  It's hard to even hear.\n",
    "\n",
    "To complete the experiment, we can also attempt to pull out the sub-bottom profiler.  This is actually a [hyperbolic down-chirp](https://en.wikipedia.org/wiki/Chirp) (i.e., it starts at the highest frequency and then moves smoothly to the lowest frequency over a short period of time - which is just about audible), which is nominally 2.5-7kHz, giving us the corner frequency specification for another band-pass filter, just like that above:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "corner_freq = [2500, 6500]\n",
    "bp_imp = signal.firwin(filter_length, corner_freq, window='blackmanharris', fs=record.framerate,  pass_zero=False)\n",
    "sig_sbp = record.convolve(bp_imp)\n",
    "disp.show_spectrogram(sig_sbp)\n",
    "sig_sbp.make_audio()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here, the suppression isn't as good as that for the multibeam (you can still hear the ship, and particularly the steam-valve), primarily because the ship noise extends more strongly into the range required for the sub-bottom profiler.  You could adjust the lower corner frequency to give better suppression, but this would also remove the bottom of the sub-bottom profiler data, compromising the performance of the sonar.  In fact, chirp systems like this depend strongly on the shape of the signal in order to achieve the best signal compression, and changing the signal like this would be a Bad Idea.\n",
    "\n",
    "# Other Things To Try\n",
    "\n",
    "- Adjust the corner frequencies for the filters, and see what effect it makes to the audible signal, and the spectrograms.\n",
    "- Generate a high-pass filter that has a corner frequency at about 15.5kHz, so that you isolate the upper level signal (from the EK80).  See if you can hear the signal, and determine what sort of signal it is.\n",
    "- If you adjust the parameters of the disp.show_spectrogram() function (you'll need to dig into the code to do this), you might be able to get a better view of the sub-bottom profiler chirp, and see the hyperbolic shape of the down-chirp.\n",
    "- Look again at the spectrogram for the multibeam echosounder data.  You'll notice that it isn't really one transmission like the sub-bottom profiler, but a collection of transmissions in a specific order over time, and in frequency.  Try to find out why this is the case ...\n",
    "- Look at the frequencies higher than the main multibeam echosounder data, but at the same time.  At about twice the frequency you may be able to see an echo of the main transmit, and at about three times the frequency, another (fainter) copy.  These are harmonics of the primary transmit, in the same way we saw harmonics in the square wave example.  Try to work out why these are here, and what they imply.\n",
    "\n",
    "# Copyright and License\n",
    "\n",
    "Copyright 2019, University of New Hampshire, Center for Coastal and Ocean Mapping.\n",
    "\n",
    "This program is free software: you can redistribute it and/or modify\n",
    "it under the terms of the GNU General Public License as published by\n",
    "the Free Software Foundation, either version 3 of the License, or\n",
    "(at your option) any later version.\n",
    "\n",
    "This program is distributed in the hope that it will be useful,\n",
    "but WITHOUT ANY WARRANTY; without even the implied warranty of\n",
    "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n",
    "GNU General Public License for more details.\n",
    "\n",
    "You should have received [a copy of the GNU General Public License](COPYING)\n",
    "along with this program.  If not, see [here](https://www.gnu.org/licenses/)."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
