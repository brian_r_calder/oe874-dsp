{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Notebook Intent\n",
    "\n",
    "This notebook complements `FIRFilterDesign` by considering the same problems using Infinite Impulse Response (IIR) filters, which use feedback terms in the definition equation:\n",
    "\n",
    "$$y(n) = \\sum_{i = 1}^{N} y(n-i)a(i) + \\sum_{i = 0}^{M-1} x(n - i)b(i)$$\n",
    "\n",
    "in order to provide more powerful filters.  There are consequences to this choice, of course, including non-linear phase effects, and potential for instability.  The feedback terms also give the filter their name, since they can cause the response of the filter to an impulse input (i.e., $x(n) = \\delta(n)$) to last, in theory, for an infinitely long time.  By contrast, since the impulse response of an FIR filter is defined exactly by the $b(i)$ coefficients, which are finite in length, the impulse response can only be of finite length.\n",
    "\n",
    "The notebook considers how to specify and implement the filters, compares them against FIR equivalents, and investigates the side-effects of the feedback terms.  Finally, it constructs equivalent filters to handle the hydrophone filtering problem.\n",
    "\n",
    "For copyright and license information, see footnote.\n",
    "\n",
    "# Resources Required\n",
    "\n",
    "We need [ThinkDSP](https://greenteapress.com/wp/think-dsp/) for signal creation, NumPy for standard arrays, SciPy's signal processing toolbox, and support code (`iir_filter_design_plots`) to display the results:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import thinkdsp as dsp\n",
    "import numpy as np\n",
    "from scipy import signal\n",
    "import iir_filter_design_plots as disp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Simple IIR Filters\n",
    "\n",
    "Unlike FIR filters, where there's a semi-standard form for design (with a choice of windows), IIR filters come in specific forms, based on analog (continuous time) prototypes that can be converted into digital equivalents.  Luckily, the tedious computations required to do this are generally hidden in the design code in `scipy.signal` and therefore don't overly concern us here.\n",
    "\n",
    "To design filters, we need the same specification of a sampling frequency and corner frequency, and a filter order:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sampling_frequency = 8000.0 # Hertz\n",
    "corner_frequency = 2000.0   # Hertz\n",
    "filter_order = 8"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note here that the filter order is quite a bit smaller than would be typical for an FIR filter.  As you'll see, however, the filter can still achieve equivalent performance as longer FIR filters even with fewer terms, due to the use of feedback from the previous output values (i.e., the $a(i)$ coefficients).\n",
    "\n",
    "There are a number of different IIR filters, each of which have their own parameters, and design code.  We'll consider three representative ones here:\n",
    "1. Butterworth filter (`signal.butter()`).  The Butterworth filter is maximally flat in pass-band and smoothly transitions to the stop-band.  It has no parameters, and is probably the lowest performing of any other designs for a given filter order.  However, it is often used as a reference standard against which others can be compared.\n",
    "2. The Chebyshev Type-I (`signal.cheby1()`).  The Chebyshev Type-I filter is a solution to an optimal estimation problem that allows for some ripple in the frequency response in the pass-band in order to give a faster transition to the stop band (which is monotone decreasing).  The design parameter is the ripple (in dB) in the pass-band.\n",
    "3. The Elliptical filter (`signal.ellip()`).  The Elliptical filter has ripple in both the pass-band and stop-band, although with a guarantee that the ripple in the stop-band never exceeds the design value (i.e., all of the sidelobes have at most the same magnitude).  The design parameters are the ripple level in the pass-band (in dB), and the minimum sidelobe level in the stop-band (in dB).\n",
    "\n",
    "Given knowledge of the required parameters, however, the design process is no more complex than for FIR filters:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "feedback_coeffs = []\n",
    "input_coeffs = []\n",
    "filter_names = []\n",
    "\n",
    "b, a = signal.butter(filter_order, corner_frequency, fs = sampling_frequency)\n",
    "input_coeffs.append(b)\n",
    "feedback_coeffs.append(a)\n",
    "filter_names.append('Butterworth')\n",
    "\n",
    "b, a = signal.cheby1(filter_order, 5.0, corner_frequency, fs = sampling_frequency)\n",
    "input_coeffs.append(b)\n",
    "feedback_coeffs.append(a)\n",
    "filter_names.append('Chebychev Type-I, 5.0dB Ripple')\n",
    "\n",
    "b, a = signal.ellip(filter_order, 5.0, 60.0, corner_frequency,fs = sampling_frequency)\n",
    "input_coeffs.append(b)\n",
    "feedback_coeffs.append(a)\n",
    "filter_names.append('Elliptical 5.0dB Ripple, 60dB Cut')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Just like FIR filters, the IIR filters can be visualized through their coefficients:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "disp.display_iir_filters(input_coeffs, feedback_coeffs, sampling_frequency, title=filter_names)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "but the frequency spectra are more useful for insight into the behaviors of the filters:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "disp.display_iir_spectra(input_coeffs, feedback_coeffs, sampling_frequency, legend=filter_names, logrange=(-100, 5))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The linear spectra show the ripple in the pass-band clearly, but fail to show the ripple in the stop-band for the Elliptical filter: the variations are too small to see.  The log-scale (dB) plots are therefore more commonly used.\n",
    "\n",
    "Note how the responses reflect the generic descriptions of the filters: the Butterworth filter is maximally smooth, but transitions from pass-band to stop-band relatively slowly compared to the others.  The Chebyshev Type-I filter transitions more quickly, and has ripple, but never more than 5dB.  Finally, the Elliptical filter has ripple in pass-band (but never more than 5dB) and has sidelobe suppression of 60dB in the stop-band for all sidelobes.\n",
    "\n",
    "# Comparison of IIR and FIR Filters\n",
    "\n",
    "We can conside FIR filters as a special case of IIR filters with $a(0) = 1$ and $a(i) = 0 \\;\\forall i > 0$.  If we design an FIR filter for the same corner frequency, we can then compare the behaviors of the filters.  For simplicity, stick with a low-pass filter with Blackman-Harris window and arbitrary length $N = 31$:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fir_filter_order = 31\n",
    "\n",
    "feedback_coeffs = []\n",
    "input_coeffs = []\n",
    "filter_names = []\n",
    "\n",
    "b = signal.firwin(fir_filter_order, corner_frequency, window='blackmanharris', fs=sampling_frequency)\n",
    "a = np.ones(1)\n",
    "feedback_coeffs.append(a)\n",
    "input_coeffs.append(b)\n",
    "filter_names.append('Blackman-Harris FIR N = %d' % (fir_filter_order,))\n",
    "\n",
    "b, a = signal.butter(filter_order, corner_frequency, fs=sampling_frequency)\n",
    "feedback_coeffs.append(a)\n",
    "input_coeffs.append(b)\n",
    "filter_names.append('Butterworth IIR N = %d' % (filter_order,))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Comparison of the spectra is the most useful model for comparison:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "disp.display_spectra_comparison(input_coeffs, feedback_coeffs, sampling_frequency, filter_names, logrange=(-100, 5))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The magnitude part of the spectrum is relatively similar between FIR and IIR, but the phase component - shown here for the first time, but present in all filters - shows a significant difference.  This difference is one of the distinguishing features of the different filter types.\n",
    "\n",
    "FIR filters, uniformly, have linear phase (the jumps at high frequency are caused when the response goes through zero, which is just out of vertical range in the magnitude plot); IIR filters have, generally, non-linear phase.  Since phase is equivalent to time delay for a single frequency, linear phase means that different frequencies receive delay proportional to frequency, so that the relative phase between different components is preserved (this is also called \"constant group delay\").  This ensures that the shapes of signals are preserved, which can be very important in applications (e.g., in motion sensor signals, where phase shift would change the interpretation of the motion measured).\n",
    "\n",
    "IIR filters, due to their non-linear phase, can cause significant differences in shape, which we can see by looking at similar length filters applied to a standard shape, for example a square wave.  To show this, start with an FIR and IIR filter with the same length and corner frequency:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "filter_order = 10\n",
    "sampling_frequency = 8000\n",
    "\n",
    "feedback_coeffs = []\n",
    "input_coeffs = []\n",
    "filter_names = []\n",
    "\n",
    "b = signal.firwin(filter_order, corner_frequency, window='blackmanharris', fs=sampling_frequency)\n",
    "input_coeffs.append(b)\n",
    "feedback_coeffs.append(np.ones(1))\n",
    "filter_names.append('Blackman-Harris FIR N = %d' % (filter_order,))\n",
    "\n",
    "b, a = signal.butter(filter_order, corner_frequency, fs=sampling_frequency)\n",
    "input_coeffs.append(b)\n",
    "feedback_coeffs.append(a)\n",
    "filter_names.append('Butterworth IIR N = %d' % (filter_order,))\n",
    "\n",
    "disp.display_spectra_comparison(input_coeffs, feedback_coeffs, sampling_frequency, filter_names, logrange=(-100, 5))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It's clear that the filters have very different responses in magnitude, with the IIR filter transitioning from pass-band to stop-band much more quickly, and staying closer to unit gain (0dB) until much closer to the corner frequency at 2kHz.  The phase response of the IIR filter is mildly non-linear, mostly around the corner frequency, but is approximately linear well inside the pass-band and stop-band.\n",
    "\n",
    "To examine the behavior, we can apply each filter to a 400Hz square wave:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "signal_frequency = 400.0\n",
    "\n",
    "source = dsp.SquareSignal(freq=signal_frequency, amp=1.0)\n",
    "wave = source.make_wave(framerate=sampling_frequency)\n",
    "\n",
    "filtered_signals = []\n",
    "signal_names = []\n",
    "\n",
    "filtered_signals.append(wave.ys)\n",
    "signal_names.append('Original, Square %.2f Hz' % (signal_frequency,))\n",
    "\n",
    "for f in range(len(filter_names)):\n",
    "    filtered = signal.lfilter(input_coeffs[f], feedback_coeffs[f], wave.ys)\n",
    "    filtered_signals.append(filtered)\n",
    "    signal_names.append('Filtered: ' + filter_names[f])\n",
    "\n",
    "disp.display_wave_comparison(wave.ts, filtered_signals, signal_names)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Clearly, the results are quite different.  Each output is shifted later in time relative to the input (known as \"phase lag\"), which is a consequency of real-world implementable filters (i.e., filters that are *causal*, meaning that they look only at the current and previous values of input and output).  The results from the FIR filter clearly has the edges rounded off, and has non-ideal transitions from $y(n) = +1$ to $y(n) = -1$; the overall shape, however, is preserved.\n",
    "\n",
    "The output of the IIR filter, on the other hand, now has an asymmetric shape, due to the non-linear phase response.  Note, however, that the non-linearity is in the vicinity of 2kHz, but the first harmonic of the aquare wave in this region is the fifth, which has magnitude 0.2 relative to the funamental; it doesn't take much of a shift to change shape.\n",
    "\n",
    "Although it's a side-effect here, precise control of phase response is critical to things like beamforming, where phase shift (or time shift) is used to line up partial responses from different acoustic elements to steer acoustic beams in different directions relative to the array.\n",
    "\n",
    "# Filtering the Hydrophone Data\n",
    "\n",
    "So long as we're not so concerned about phase, however, we can generate filters for ship noise suppression and sonar selection with IIR primitives much as we did for the FIR filters.  Because of the extra performance from the feedback terms, however, we don't need anything like the same number of coefficients:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "record = dsp.read_wave('hydrophone.wav')\n",
    "\n",
    "sampling_frequency = record.framerate\n",
    "iir_filter_length = 10\n",
    "fir_filter_length = 201\n",
    "\n",
    "record.make_audio()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There is, however, a small caveat in designing IIR filters of any appreciable length, which we'll consider later.  For the time being, it's sufficient to know that there are multiple different ways to compute the filter representation rather than computing the $a(i), b(i)$ directly as we typically do for FIR filters, and have done for IIR filters up until now, and some are more numerically stable than others.  In the following example, therefore, we use the \"zero-pole\" model of the filter (`output='zpk'`) rather than the default \"transfer function\" model.  For the FIR filter, we use the standard transfer function model, but then convert to zero-pole model (`signal.tf2zpk()`) in order to allow for direct comparison between the filters.\n",
    "\n",
    "For the IIR filter, the code here uses the Chebyshev Type-II filter, which has ripple in the stop-band, but is flat in the pass-band.  This ensures that we get guaranteed gain reduction from pass-band to stop-band, but the design point for the Chebyshev Type-II is where the frequency response starts to transition from stop-band into the pass-band, rather than where it gets to the pass-band, which means that we need to have different corner frequencies for the FIR and IIR filters; the corner frequencies are chosen empirically to best match the FIR results:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "filter_spec = []\n",
    "filter_names = []\n",
    "\n",
    "corner_freq = 2500.0\n",
    "b = signal.firwin(fir_filter_length, corner_freq, window='blackmanharris', fs=sampling_frequency, pass_zero=False)\n",
    "z, p, k = signal.tf2zpk(b, np.ones(1))\n",
    "filter_spec.append((z, p, k))\n",
    "filter_names.append('FIR Ship Suppression')\n",
    "\n",
    "corner_freq = 1165.0\n",
    "z, p, k = signal.cheby2(iir_filter_length, 120.0, corner_freq, btype='highpass', fs=sampling_frequency, output='zpk')\n",
    "filter_spec.append((z, p, k))\n",
    "filter_names.append('IIR Ship Suppression')\n",
    "\n",
    "corner_freq = [9500.0, 15500.0]\n",
    "b = signal.firwin(fir_filter_length, corner_freq, window='blackmanharris', fs=sampling_frequency, pass_zero=False)\n",
    "z, p, k = signal.tf2zpk(b, np.ones(1))\n",
    "filter_spec.append((z, p, k))\n",
    "filter_names.append('FIR MBES Bandpass')\n",
    "\n",
    "corner_freq = [7300.0, 19460.0]\n",
    "z, p, k = signal.cheby2(iir_filter_length, 120.0, corner_freq, btype='bandpass', fs=sampling_frequency, output='zpk')\n",
    "filter_spec.append((z, p, k))\n",
    "filter_names.append('IIR MBES Bandpass')\n",
    "\n",
    "corner_freq = [2500.0, 6500.0]\n",
    "b = signal.firwin(fir_filter_length, corner_freq, window='blackmanharris', fs=sampling_frequency, pass_zero=False)\n",
    "z, p, k = signal.tf2zpk(b, np.ones(1))\n",
    "filter_spec.append((z, p, k))\n",
    "filter_names.append('FIR SBP Bandpass')\n",
    "\n",
    "corner_freq = [1600.0, 10000.0]\n",
    "z, p, k = signal.cheby2(iir_filter_length, 120.0, corner_freq, btype='bandpass', fs=sampling_frequency, output='zpk')\n",
    "filter_spec.append((z, p, k))\n",
    "filter_names.append('IIR SBP Bandpass')\n",
    "\n",
    "disp.display_paired_spectra_comparison(filter_spec, sampling_frequency, filter_names, logrange =(-200, 5))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Applying the IIR filters is a little more complex than for the FIR, where the impulse response is defined entirely by the $b(i)$ coefficients, and therefore a simple convolution is sufficient.  The IIR filters, by definition, don't have an impulse function that we can easily manipulate (due to the feedback terms), so we need an alternative approach.\n",
    "\n",
    "There are a number of alternatives, but the one supported by `scipy.signal` is to convert the zero-pole description of the filter into an alternative form called a \"second order section\" (SOS, `scipy.signal.zpl2sos()`), and then use that description for the filtering (`scipy.signal.sosfilt()`).  The SOS form takes the fitler description and breaks it down into smaller sections (a \"biquadratic\" or [\"biquad\"](https://en.wikipedia.org/wiki/Digital_biquad_filter) filter) that can be linked together to give the same effect as the original response.  The advantage of an SOS structure is that it's much more robust than the alternatives, which can be a problem with longer filter designs.\n",
    "\n",
    "In practice, it's something that's just implemented in the support library, and the effects of the filter are much the same as for the FIR form:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hpf = signal.zpk2sos(filter_spec[1][0], filter_spec[1][1], filter_spec[1][2])\n",
    "sig_hp = signal.sosfilt(hpf, record.ys)\n",
    "sig_wave = dsp.Wave(sig_hp, framerate = record.framerate)\n",
    "disp.show_spectrogram(sig_wave)\n",
    "sig_wave.make_audio()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Of course, human ears aren't particularly sensitive to phase in this sense (although phase difference is why we have two ears and can tell where sound is coming from around us), so the effects of this filtering on a sonar receiver would be an interesting investigation.\n",
    "\n",
    "Selecting the multibeam sonar is essentially the same process:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mbes_f = signal.zpk2sos(filter_spec[3][0], filter_spec[3][1], filter_spec[3][2])\n",
    "sig_mbes = signal.sosfilt(mbes_f, record.ys)\n",
    "sig_wave = dsp.Wave(sig_mbes, framerate = record.framerate)\n",
    "disp.show_spectrogram(sig_wave)\n",
    "sig_wave.make_audio()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "as is selecting the sub-bottom sonar response:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sbp_f = signal.zpk2sos(filter_spec[5][0], filter_spec[5][1], filter_spec[5][2])\n",
    "sig_sbp = signal.sosfilt(sbp_f, record.ys)\n",
    "sig_wave = dsp.Wave(sig_sbp, framerate = record.framerate)\n",
    "disp.show_spectrogram(sig_wave)\n",
    "sig_wave.make_audio()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Filter Stability\n",
    "\n",
    "One significant consequence of the feedback, $a(i)$, coefficients in an IIR filter is that they are only conditionally stable: there exists the possibility that including previous samples from the output will cause the system to become unstable, much like a microphone held too close to the speaker can amplify noise until the whole system screeches.  In practice, what you typically see is the output of the filter spontaneously breaking into oscillations at steadily increasing amplitude.\n",
    "\n",
    "The standard designs of IIR filter (Butterworth, Chebyshev Type-II and II, Elliptic, etc.) are all normally stable due to their internal structure.  However, the methods by which they are computed (e.g., by `scipy.signal.butter()`) are only stable if there is perfect arithmetic; in a computer, where the accuracy of the arithmetic is very high, but not perfect, there is finite possibility of there being numerical round-off or error propagation that causes the filter to misbehave, or at least fail to meet its design specification.  This is particularly true when the filter is computed directly in transfer function form (i.e., where the $a(i)$ and $b(i)$ are returned).\n",
    "\n",
    "For example, consider the design of the band-pass filter to select the sub-bottom profiler data.  If we design using the standard method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "filter_spec = filter_spec[4:]\n",
    "filter_names = filter_names[4:]\n",
    "corner_freq = [1600.0, 10000.0]\n",
    "b, a = signal.cheby2(iir_filter_length, 120.0, corner_freq, btype='bandpass', fs=sampling_frequency)\n",
    "z, p, k = signal.tf2zpk(b, a)\n",
    "filter_spec.append((z, p, k))\n",
    "filter_names.append('Bad IIR SBP Bandpass')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "the computation completes as expected, and if you examine the coefficients, there's nothing particularly suggestive of there being an issue with the results.  However, if you compare the result to the filter computed with the same parameters, but using zero-pole form, the results are striking:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "disp.display_filter_comparison(filter_spec, sampling_frequency, filter_names, logrange=(-200, 5))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here, it's clear that the \"bad\" filter does not meet the lower end of the pass-band requirement, and has very odd phase response in the same region.  The upper transition from pass-band to stop-band, however, is the same as for the \"good\" filter design.  This difference is purely to do with the way in which the computation for the filter design was done - but there's no diagnostic from the filter coefficients themselves to tell that this has happened.  Having band-edges (low-pass, band-pass, or high-pass) close to either zero or Nyquist frequency tends to make this problem more likely.\n",
    "\n",
    "Interestingly, the difference is in the transfer function form: even if you design in zero-pole form and then convert to transfer function form, you'll get the same problem.\n",
    "\n",
    "In practice, the simplest way to avoid this is to simply use zero-pole, or SOS forms when designing.\n",
    "\n",
    "# Other Things to Try\n",
    "\n",
    "- Examine the standard forms of filters for the same corner and sampling frequency, and compare the results.  Look, particularly, at the transition rate, and the potential for stop-band suppression.\n",
    "- For a standard filter design, generate filters with order 2, 5, and 10 (or more, if you like), and plot together to see the effects of increasing order.  Look particularly at the transition and the phase response.\n",
    "- A more precise way to look at phase shifts is the [Group Delay](https://en.wikipedia.org/wiki/Group_delay_and_phase_delay) of a filter or system, which shows the slope of the phase response.  Systems with constant group delay have linear phase; the extent to which the group delay varies reflects the level of non-linearity.  You can compute group delay with `scipy.signal.group_delay()`.  Do this for a Butterworth, Chebyshev Type-I, and Elliptical filter; compare against any FIR filter (which automatically have linear phase response).\n",
    "\n",
    "# Copyright and License\n",
    "\n",
    "Copyright 2019, University of New Hampshire, Center for Coastal and Ocean Mapping.\n",
    "\n",
    "This program is free software: you can redistribute it and/or modify\n",
    "it under the terms of the GNU General Public License as published by\n",
    "the Free Software Foundation, either version 3 of the License, or\n",
    "(at your option) any later version.\n",
    "\n",
    "This program is distributed in the hope that it will be useful,\n",
    "but WITHOUT ANY WARRANTY; without even the implied warranty of\n",
    "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n",
    "GNU General Public License for more details.\n",
    "\n",
    "You should have received [a copy of the GNU General Public License](COPYING)\n",
    "along with this program.  If not, see [here](https://www.gnu.org/licenses/)."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
