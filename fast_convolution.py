#   Copyright 2019, University of New Hampshire, Center for Coastal and Ocean Mapping.
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

import thinkdsp as dsp
from scipy import signal
import fast_convolution_plots as disp


# So that they're visible everywhere (and changeable in one place), set up the
# parameters for the example
samp_freq = 8000
signal_duration = 1.0
signal_freq = 440
filter_length = 101

signal_model = dsp.SinSignal(freq=signal_freq, amp=1.0, offset=0.0)
noise_model = dsp.UncorrelatedGaussianNoise(amp=0.5)

pure = signal_model.make_wave(duration=signal_duration, framerate=samp_freq)
noise = pure + noise_model.make_wave(duration=signal_duration, framerate=samp_freq)

disp.display_start_point(pure, noise, save='fast_conv_startpoint')

# Generate the filter to bandpass the data around the known position
corner_freq = [0.9*signal_freq, 1.1*signal_freq]
bp_imp = signal.firwin(filter_length, corner_freq, fs=samp_freq, pass_zero=False)
bp_filter = dsp.Wave(bp_imp, framerate=samp_freq)

# Filter with linear and fast convolution (with appropriate padding)
linear_conv = noise.convolve(bp_filter)

bp_padded = bp_filter.copy()
bp_padded.zero_pad(len(bp_filter) + len(noise) + 1)
bp_spec = bp_padded.make_spectrum()

noise_padded = noise.copy()
noise_padded.zero_pad(len(bp_filter) + len(noise) + 1)
noise_spec = noise_padded.make_spectrum()

circular_spec = bp_spec * noise_spec
circular_conv = circular_spec.make_wave()

disp.display_padded(bp_filter, noise, linear_conv, circular_conv, save='fast_conv_padded')

# To show the effects of filtering without padding, we only pad out to the length
# of the signal, then re-filter

bp_short_padded = bp_filter.copy()
bp_short_padded.zero_pad(len(noise))
bp_short_spec = bp_short_padded.make_spectrum()

noise_short_spec = noise.make_spectrum()

circular_short_spec = bp_short_spec * noise_short_spec
circular_short_conv = circular_short_spec.make_wave()

disp.display_error_signal(noise, linear_conv, circular_conv, circular_short_conv, save='fast_conv_comparison')
