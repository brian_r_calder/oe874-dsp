{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Notebook Intent\n",
    "\n",
    "Finite Impulse Response (FIR) filters are one of the fundamental building blocks for digital signal processing, representing the implementation of a filter that acts only of the input signal sequence in order to form the output.  They implement the equation:\n",
    "\n",
    "$$y(n) = \\sum_{i = 0}^{N-1} x(n - i)b(i)$$\n",
    "\n",
    "so that they are completely defined by their parameters $b(i)$.  How to choose the $b(i)$ to give specific effect, and how these systems behave, are the topics of this notebook.\n",
    "\n",
    "For copyright and license information, see footnote.\n",
    "\n",
    "# Required Resources\n",
    "\n",
    "We need SciPy's signal processing tools, and support code (`fir_filter_design_plots`) to show the results in appropriate form:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from scipy import signal\n",
    "import fir_filter_design_plots as disp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Ideal Low-Pass Filter\n",
    "\n",
    "To understand design of filters, it's useful to start with what an ideal filter would look like.  For a low-pass filter, this means preserving without change all of the frequencies below a given \"corner frequency\" and stopping everything above this.  The corner frequency is (almost) arbitrary:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sampling_frequency = 8000.0\n",
    "corner_frequency = 2000.0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the frequency domain, the ideal low-pass filter would be unit gain below the corner frequency, and zero above:\n",
    "\n",
    "$$H(\\omega) =\n",
    "    \\begin{cases}\n",
    "        1 & |\\omega| \\leq \\omega_c\\\\\n",
    "        0 & \\omega_c < \\omega \\leq \\pi\n",
    "    \\end{cases}$$\n",
    "\n",
    "In the time domain, this is a sinc function,\n",
    "\n",
    "$$h(n) = \n",
    "    \\begin{cases}\n",
    "        \\frac{\\omega_c}{\\pi} & n = 0\\\\\n",
    "        \\frac{\\omega_c}{\\pi}\\frac{\\sin(\\omega_c n)}{\\omega_c n} & n \\not= 0\n",
    "    \\end{cases}\n",
    "$$\n",
    "\n",
    "Unfortunately, $h(n)$ goes never quite goes to zero, and we would need an infinite length sequence (at both negative and positive $n$) to represent it.  That's not physically possible, so we need to make an approximation.\n",
    "\n",
    "In practice, this means that we limit the number of coefficients from the sinc function that we preserve, and we shift the sequence by half this amount to make sure that we don't need to look ahead (negative indices) to do the computation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fir_lengths = [11, 31, 51]\n",
    "filters = []\n",
    "\n",
    "for coeffs in fir_lengths:\n",
    "    fir = signal.firwin(coeffs, corner_frequency, window='boxcar', fs=sampling_frequency)\n",
    "    filters.append(fir)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here, the `window=boxcar` parameter chooses the ideal filter response in frequency (we'll come back to alternatives in a minute).  The outputs from these computations are just different lengths of the same signal:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "disp.display_fir_filters(filters, sampling_frequency)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "so it isn't clear what the different lengths do.  If we look at the spectra of the filters, however, they show the differences between the filters, and the difficulties that the approximation of finite length entail:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "disp.display_fir_spectra(filters, sampling_frequency)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here, the linear magnitude (top) and log-magnitude (bottom) clearly demonstrate that as the filter length increases, the transition between pass-band (below the corner) to stop-band (above the corner) becomes steeper, making them a better approximation to the ideal (which would step down instantly from value 1.0 to 0.0).\n",
    "\n",
    "It's also clear, however, that the finite length approximation has generated ripples in the response in the pass/stop-bands, making the filter non-ideal.  Note that the number of ripples increases with length, but the magnitude is not decreasing with filter length, and the first peak after the transition (\"first sidelobe\") is always at the same level: it doesn't matter how long we make the sequence, this problem isn't going away.  This is an example of [Gibbs Phenomenon](https://en.wikipedia.org/wiki/Gibbs_phenomenon), and happens because we are attempting to approximate a step change (in frequency).\n",
    "\n",
    "# Alternative Designs\n",
    "\n",
    "The ripples in the design are not ideal, and practical implementations of filters need to do better.  The goal is usually to reduce the level of ripple in the pass-band, and to reduce the level of the sidelobes in the stop-band, particularly the first, which is generally the most significant for its effect.\n",
    "\n",
    "The problem occurs because we truncated the sinc function approximation sharply in the time domain (i.e., we just stopped taking new coefficients).  This means that there are discontinuities in the filter representation, which we want to avoid.  One way to do this is to smooth off the coefficients as we head towards the ends of the sequence, so that they roll off to zero at the edges (or close enough); we can do this by multiplying the coefficients by a function that is value 1.0 at the center, but drops off to zero towards the edges.  Such a function is called a \"window\".\n",
    "\n",
    "The process of applying the window is called \"windowing\", and crops up in many different contexts including power spectrum density estimation and multibeam beamforming.  The default condition above can be considered a window that is unit value across the entire range of the sequence, and then drops to zero (this is called a \"rectangular\" or (American usage) \"boxcar\" window), but there are many different alternatives.  By way of example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "window_names = ['boxcar', 'hamming', 'blackmanharris']\n",
    "window_labels = ['Rectangular', 'Hamming', 'Blackman-Harris']\n",
    "windows = []\n",
    "for w in range(len(window_names)):\n",
    "    windows.append(signal.get_window(window_names[w], 101))\n",
    "\n",
    "disp.display_fir_filters(windows, sampling_frequency, ylabel='Window Magnitude', linestyle='k-', legend=window_labels)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Each of these windows helps to reduce the ripples, and suppress the sidelobes, which we can see if we compute filters using each of the windows in turn:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "filters = []\n",
    "for w in range(len(window_names)):\n",
    "    fir = signal.firwin(51, corner_frequency, window = window_names[w], fs = sampling_frequency)\n",
    "    filters.append(fir)\n",
    "disp.display_fir_filters(filters, sampling_frequency, legend=window_labels)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The time-domain representation isn't terribly useful for this sort of analysis (it's actually only rarely used), but we can convert into the time domain and look at the spectra instead:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "disp.display_fir_spectra(filters, sampling_frequency, legend=window_labels, logrange=(-100, 5))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Clearly, both Hamming and Blackman-Harris windows reduce the pass-band ripple and the magnitude of the first sidelobe (in the case of the Blackman-Harris, below -100dB) compared to the \"standard\" rectangular window.  Note, however, that the effect isn't free: the rate at which the filter rolls off from pass-band to stop-band is decreased for Hamming window, and more so for the Blackman-Harris.  There's no such thing as a free lunch: the more the sidelobe and ripple are suppressed, the more the transition is affected.\n",
    "\n",
    "There are design choices to be made, therefore, in the trade-off between suppression level and transition width in selecting which window to use.  In many cases, however, it may be sufficient to choose the Hamming window as a good compromise (it's often the default choice in design software), but the Blackman-Harris window also provides very good performance for minimal extra transition width.  Another specialist windows is the Chebyshev, which guarantees that none of the sidelobes will exceed a given level (often -100dB), and is often used in beamforming.\n",
    "\n",
    "# Filtering the Hydrophone Data\n",
    "\n",
    "Windowed FIR filter design is simple, but readily understandable, and easy to automate.  It's therefore useful when you want to set up a quick filter and you're not too fussy about the specifics of the filter and efficiency of implementation isn't an issue.  (We'll come back to both of those when thinking about the alternative standard filter form, Infinite Impulse Response, or IIR, filters.)\n",
    "\n",
    "By way of example, consider the problem of filtering the hydrophone data.  Here, we're not (initially at least) concerned about filter shape or efficiency, so we can set up simple high-pass and band-pass filters to suppress the ship noise and select sonar signals, respectively.  The signal was sampled at 96kHz, and we want a relatively long filter so that we get good selectivity:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sampling_frequency = 96000.0\n",
    "filter_length = 201"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Suppressing the ship noise means eliminating the low-frequency noise, meaning that we need a high-pass filter at around 2.5kHz.  Selecting `pass_zero=False` tells the code to not pass zero frequency, making this effectively a high-pass filter:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "corner_freq = 2500.0\n",
    "hp_imp = signal.firwin(filter_length, corner_freq, window='blackmanharris', fs=sampling_frequency, pass_zero=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Selecting the sonar signals is a band-pass filtering (we want to preserve a range of frequencies).  We need to specify the start and stop frequency, and again select `pass_zero=False` to tell the code not to pass zero frequency, so that the first frequency specified in `corner_freq` is where the filter first heads into its pass-band.  We need one for the multibeam (MBES) and another for the sub-bottom profiler:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "corner_freq = [9500.0, 15500.0]\n",
    "mbes_imp = signal.firwin(filter_length, corner_freq, window='blackmanharris', fs=sampling_frequency, pass_zero=False)\n",
    "\n",
    "corner_freq = [2500.0, 6500.0]\n",
    "subbottom_imp = signal.firwin(filter_length, corner_freq, window='blackmanharris', fs=sampling_frequency, pass_zero=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The different frequency responses of the filters let us confirm that we got the design parameters correct:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "filters = [hp_imp, mbes_imp, subbottom_imp]\n",
    "filter_descriptions = ['Ship Suppression', 'MBES Bandpass', 'SBP Bandpass']\n",
    "disp.display_fir_spectra(filters, sampling_frequency, legend=filter_descriptions, logrange=(-200, 5))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The spectra clearly show that the Ship Suppression filter has at least -100dB response as it heads towards zero frequency (often called - somewhat incorrectly - \"d.c.\" in analogy to \"direct current\", which would make sense if we were actually measuring currents!), but unit gain above 2.5kHz, allowing all of the other data through.  The MBES bandpass filter selects the region 9.5-15.5kHz as required, and the sub-bottom bandpass filter selects 2.5-6.5kHz.\n",
    "\n",
    "These filters were simply applied to the hydrophone signal using the `convolve()` function (which is in SciPy but can also be accessed through ThinkDSP's `Wave` object) to generate the examples.  See the `HydrophoneFiltering` notebook for more details.\n",
    "\n",
    "# Other Things to Try\n",
    "\n",
    "- See what happens if you use other windows than the Blackman-Harris for designing the filters: do the filters behave differently?  Are the effects significantly different?\n",
    "- Apply the filters you design to the hydrophone data; is there an audible difference?  Do you think there is a better separation between the signals?\n",
    "- If you change the length of the filter (shorter and longer), can you get better responses?  *The filter length controls the rate of change from pass-band to stop-band, but longer filters cost more to implement (in compute cycles).  If you've got a lot of data to process, that becomes important, so there's a design trade-off here.*\n",
    "- The same filter length as a high-pass and band-pass filter has different transitions; design a high-pass and band-pass filters for the same (lower) corner frequency and see how much they are affected.\n",
    "- There are other ways to generate FIR filters, including specification of the frequency response directly, and an optimal design (\"Parks-McClellan\") algorithm.  These are both implemented in `scipy.signal` as `firwin2()` and `remez()` respectively.  Investigate the benefits and problems of these as alternative design methods.\n",
    "\n",
    "# Copyright and License\n",
    "\n",
    "Copyright 2019, University of New Hampshire, Center for Coastal and Ocean Mapping.\n",
    "\n",
    "This program is free software: you can redistribute it and/or modify\n",
    "it under the terms of the GNU General Public License as published by\n",
    "the Free Software Foundation, either version 3 of the License, or\n",
    "(at your option) any later version.\n",
    "\n",
    "This program is distributed in the hope that it will be useful,\n",
    "but WITHOUT ANY WARRANTY; without even the implied warranty of\n",
    "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n",
    "GNU General Public License for more details.\n",
    "\n",
    "You should have received [a copy of the GNU General Public License](COPYING)\n",
    "along with this program.  If not, see [here](https://www.gnu.org/licenses/)."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
