#   Copyright 2019, University of New Hampshire, Center for Coastal and Ocean Mapping.
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

import matplotlib.pyplot as plt
from scipy import signal
import fir_filter_design_plots as disp

sampling_frequency = 8000.0 # Hertz
corner_frequency = 2000.0   # Hertz

# Generate truncated rectangular window filters (i.e., the "realistic version of
# the nominally ideal filter") to show how they behave with respect to the
# corner frequency and Gibbs phenomenon

fir_lengths = [11, 31, 51]
filters = []

for coeffs in fir_lengths:
    fir = signal.firwin(coeffs, corner_frequency, window='boxcar', fs=sampling_frequency)
    filters.append(fir)
    
disp.display_fir_filters(filters, sampling_frequency, save='boxcar-filters-coeffs')
disp.display_fir_spectra(filters, sampling_frequency, save='boxcar-filters-spectra')

# Generate a couple of different windows to show the difference between the
# rectangular window (i.e., no real window) and better versions.

window_names = ['boxcar', 'hamming', 'blackmanharris']
window_labels = ['Rectangular', 'Hamming', 'Blackman-Harris']
windows = []
for w in range(len(window_names)):
    windows.append(signal.get_window(window_names[w], 101))

disp.display_fir_filters(windows, sampling_frequency, ylabel='Window Magnitude', linestyle='k-', legend=window_labels, save='window-functions')

# Generate FIR filters with each of the windows used so that we can see the
# difference in shape

filters = []
for w in range(len(window_names)):
    fir = signal.firwin(51, corner_frequency, window = window_names[w], fs = sampling_frequency)
    filters.append(fir)

disp.display_fir_filters(filters, sampling_frequency, legend=window_labels, save='windowed-fir-coeffs')
disp.display_fir_spectra(filters, sampling_frequency, legend=window_labels, logrange=(-100, 5), save='windowed-fir-spectra')

# Generate FIRs for the hydrophone filtering example again
sampling_frequency = 96000.0
filter_length = 201

corner_freq = 2500.0
hp_imp = signal.firwin(filter_length, corner_freq, window='blackmanharris', fs=sampling_frequency, pass_zero=False)

corner_freq = [9500.0, 15500.0]
mbes_imp = signal.firwin(filter_length, corner_freq, window='blackmanharris', fs=sampling_frequency, pass_zero=False)

corner_freq = [2500.0, 6500.0]
subbottom_imp = signal.firwin(filter_length, corner_freq, window='blackmanharris', fs=sampling_frequency, pass_zero=False)

filters = [hp_imp, mbes_imp, subbottom_imp]
filter_descriptions = ['Ship Suppression', 'MBES Bandpass', 'SBP Bandpass']

disp.display_fir_filters(filters, sampling_frequency, legend=filter_descriptions, save='hydrophone-filter-coeffs')
disp.display_fir_spectra(filters, sampling_frequency, legend=filter_descriptions, logrange=(-200, 5), save='hydrophone-filter-spectra')

plt.show()
