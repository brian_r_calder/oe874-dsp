#   Copyright 2019, University of New Hampshire, Center for Coastal and Ocean Mapping.
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

import aliasing_spectra_plots as disp

lofi_sampling_frequency = 2000
hifi_sampling_frequency = 8000

lo, hi = disp.make_spectra(400, lofi_sampling_frequency, hifi_sampling_frequency)
disp.spectrum_comparison(lo, hi, save='alias_spectra_400Hz')

lo, hi = disp.make_spectra(900, lofi_sampling_frequency, hifi_sampling_frequency)
disp.spectrum_comparison(lo, hi, save='alias_spectra_900Hz')

lo, hi = disp.make_spectra(1100, lofi_sampling_frequency, hifi_sampling_frequency)
disp.spectrum_comparison(lo, hi, save='alias_spectra_1100Hz')

lo, hi = disp.make_spectra(1600, lofi_sampling_frequency, hifi_sampling_frequency)
disp.spectrum_comparison(lo, hi, save='alias_spectra_1600Hz')
