#   Copyright 2019, University of New Hampshire, Center for Coastal and Ocean Mapping.
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

import thinkdsp as dsp
import sampling_reconstruction_plots as disp
from scipy import interpolate

full_sampling_frequency = 16000

base = dsp.CosSignal(freq=5.0, amp=1.0, offset=0.0)
full = base.make_wave(duration=1.0, framerate=full_sampling_frequency)

samp_rates = [32, 16, 8]
sigs = []
for f in samp_rates:
    w = base.make_wave(duration=1.0, framerate=f)
    sigs.append(w)

disp.display_signals(full, sigs, save='sampled-original')

interps = ['previous', 'linear', 'sinc']
for interp in interps:
    reconstructions = []
    for s in range(len(samp_rates)):
        extended_sig = base.make_wave(duration=5.0, framerate=samp_rates[s])
        if interp == 'sinc':
            w = disp.sinc_interpolate(extended_sig.segment(start=0, duration=1.0), full.framerate)
        else:
            interpolator = interpolate.interp1d(extended_sig.ts, extended_sig.ys, kind=interp)
            w = dsp.Wave(interpolator(full.ts), ts=full.ts, framerate=full.framerate)
        reconstructions.append(w)
    disp.display_signals(full, sigs, overplot=reconstructions, save='sampled-reconstructed-' + interp)
