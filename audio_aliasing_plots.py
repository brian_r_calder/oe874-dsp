#   Copyright 2019, University of New Hampshire, Center for Coastal and Ocean Mapping.
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

import matplotlib.pyplot as plt
import dsp_aux as aux


def display_audio(signal, **kwargs):
    plt.figure(figsize=(14,10))
    plt.plot(signal.ts, signal.ys)
    if 'overplot' in kwargs:
        plt.plot(kwargs['overplot'].ts, kwargs['overplot'].ys)

    plt.grid()
    if 'trange' in kwargs:
        plt.gca().set(xlim=kwargs['trange'])

    plt.xlabel('Time (s)')
    plt.ylabel('Audio Signal Amplitude')
    plt.show()

    if 'save' in kwargs:
        aux.save_figure(kwargs['save'])
