{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Notebook Intent\n",
    "\n",
    "This notebook looks at the various methods that can be used to determine the power spectral density (PSD) for a signal, in particular considering the requirements for the signal, the difference between ergodic and non-ergodic signals, and parametric and non-parametric methods for PSD.  There are a lot of different methods that can be used to compute PSDs, so we focus here on the most common, and mention the rest.\n",
    "\n",
    "For copyright and licensing information, see the footnote.\n",
    "\n",
    "# Resources Required\n",
    "\n",
    "We need quite a range of different resources for this experiment because the non-parametric methods are better in `scipy.signal` but it doesn't have any parametric methods, which we need to import from `spectrum` (note that we pull specific routines in, rather than all).  We need [ThinkDSP](https://greenteapress.com/wp/think-dsp) for signal generation, `numpy` for logarithms, and support code (`power_spectra_plots`) for display of the results of the analyses."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import thinkdsp as dsp\n",
    "from spectrum import parma, pyule, aryule, arma2psd\n",
    "from scipy import signal\n",
    "import numpy as np\n",
    "import power_spectra_plots as disp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Basic Signal Generation\n",
    "\n",
    "We start by generating some data to use for testing.  In order to make sure that we have a single location for changing the parameters for the experiments, we collect the parameters here:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "samp_freq = 8000        # Hertz\n",
    "sig_freq = 400          # Hertz\n",
    "signal_amp = 1.0        # Arbitrary units\n",
    "sample_duration = 5.0   # seconds\n",
    "noise_sd = 3.0          # Arbitrary units"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The signal is a fixed cosine wave at the `sig_freq`, to which we then add noise.  In this case, the noise is uncorrelated Gaussian noise (also called \"Independent Identically Distributed Gaussian\" noise), with standard deviation `noise_sd`.  Note that there is no actual reference level here, but the units of standard deviation and signal amplitude are the same.  In order to minimise memory usage, we generate the theoretical signal model for the input (noisy) samples first, and only then sample it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "signal_model = dsp.CosSignal(freq=sig_freq, amp=signal_amp, offset=0.0)\n",
    "signal_samples = signal_model.make_wave(duration=sample_duration, framerate=samp_freq)\n",
    "\n",
    "noise_model = dsp.UncorrelatedGaussianNoise(amp=noise_sd)\n",
    "\n",
    "input_model = signal_model + noise_model\n",
    "input_samples = input_model.make_wave(duration=sample_duration, framerate=samp_freq)\n",
    "\n",
    "disp.display_signal([input_samples, signal_samples], ['Input Signal', 'Original Signal'],\n",
    "                    trange=(0, 0.05), srange=(-20, 20))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note here how the magnitude of the input signal (with noise) is significantly higher than the noise-free signal: we're adding a lot of noise here, and you'd be hard pressed to recognise that there's a periodic signal in the input signal without knowing about it beforehand.\n",
    "\n",
    "# Ergodic Signals\n",
    "\n",
    "When dealing with noisy signals, we have to tackle the problem of the frequency content of the noise, and in particular the problem that the noise doesn't necessarily have frequencies that fit neatly in to the frequencies that we can represent in a sampled frequency spectrum.  (Remember that we're sampling in frequency when we do a discrete spectrum, rather than a continuous spectrum, which we have to do in order to do the computation.  That means that we're assuming that the signal we're looking at is one period of a periodic signal.)\n",
    "\n",
    "A consequence of this is that in order to get reliable estimates of what's going on we'd, ideally, like to sample whatever the input signal is multiple times at each sample instant, and then look across all of these data captures to generate estimates of whatever we're interested in (e.g., the mean value, or standard deviation).  These multiple samples are, together, called an \"ensemble\" and the statistics \"ensemble averages\".  So, if we could, we would like to see multiple realizations of the same signal:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "unit_noise_signal = dsp.UncorrelatedGaussianNoise(amp=1.0)\n",
    "\n",
    "signals = []\n",
    "names = []\n",
    "for s in range(3):\n",
    "    ergodic_signal = unit_noise_signal.make_wave(duration=3.0, framerate=samp_freq)\n",
    "    signals.append(ergodic_signal)\n",
    "    names.append('Realisation %d' % (s+1,))\n",
    "\n",
    "disp.display_signal(signals, names, trange=(0, 0.05), srange=(-3.5, 3.5))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "and then average point-wise in time.\n",
    "\n",
    "Unfortunately, we very rarely get the opportunity to observe the same signal more than once - it's expensive to have multiple measuring instruments on the same signal at the same time.  Consequently, we have to make an approximation in order to make progress.  In this case, the assumption is that the signal that we record is, over time, stable with respect to the parameters (generally statistics) that we want to measure.  This is usually described as being [stationary](https://en.wikipedia.org/wiki/Stationary_process), or [ergodic](https://en.wikipedia.org/wiki/Ergodicity) (there are differences in the details, but they don't really concern us here).\n",
    "\n",
    "In practice, what this means is that an ergodic signal's parameters estimates (e.g., mean, standard deviation, or frequency content) computed over time are the same as if we had multiple samples in an ensemble.\n",
    "\n",
    "Many signals are at least partially ergodic in the sense that their parameters change slowly over time, and therefore you can assume that they're ergodic so long as you break them into segments (that aren't too long), and process each segment in turn.  By way of example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ergodic_signal = unit_noise_signal.make_wave(duration=3.0, framerate=samp_freq)\n",
    "\n",
    "sd_signal = dsp.CosSignal(freq=1.0, amp=0.5, offset=0.0)\n",
    "sd_samples = sd_signal.make_wave(duration=3.0, framerate=samp_freq)\n",
    "sd_samples.bias(1.5)\n",
    "\n",
    "non_ergodic_signal = ergodic_signal * sd_samples\n",
    "\n",
    "disp.display_signal([ergodic_signal, non_ergodic_signal],\n",
    "                    ['Ergodic Noise', 'Non-ergodic Noise'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this case, the top signal is ergodic Gaussian noise with mean 0.0 units and standard deviation 1.0 units.  The bottom signal has its standard deviation modulated so that it oscillates between 1.0 and 2.0 units at a frequency of 1Hz.  If, however, we were to take small enough sections, for example a hundred samples or so, we might plausibly assume ergodicity in order to apply PSD estimation techniques.\n",
    "\n",
    "# Basic Types of PSD Estimation\n",
    "\n",
    "Broadly speaking, there are two primary modes of PSD estimation: non-parametric and parametric models.  Non-parametric methods came first historically, and use just the data, without an underlying model of the signal, in order to estimate the PSD.  Parametric methods start with an underlying model of the structure of the signal, and then fit that model to the samples, estimating the theoretical PSD from the resulting model, rather than directly from the data.  There are various methods in both camps, but the most common non-parametric method is the [periodogram](https://en.wikipedia.org/wiki/Periodogram); as a common example of parametric methods, we'll consider the [Autoregressive Moving Average model](https://en.wikipedia.org/wiki/Autoregressive%E2%80%93moving-average_model).  An immediate example of both methods for the example data above is:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "per_f, per_Pxx = signal.periodogram(input_samples.ys, fs=samp_freq, nfft=4096)\n",
    "per = (per_f, per_Pxx)\n",
    "ar = parma(input_samples.ys, 10, 10, 30, NFFT=4096, sampling=samp_freq)\n",
    "\n",
    "# Display the whole spectrum, and a zoom around the known frequency\n",
    "disp.display_power_spectra([per, ar], ['Periodogram', 'ARMA(10) Parametric'],\n",
    "                           mrange=(-75, 25))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "which illustrates some important differences between the two methods:\n",
    "1. The non-parametric (periodogram) methods is significantly noisier than the parametric (ARMA) method.\n",
    "2. The periodogram spike at 400Hz (the signal frequency) is significantly narrower than the ARMA peak (i.e., it'd be easier to tell closely-spaced frequencies apart with this method).\n",
    "3. There are auxiliary peaks in the ARMA model which are not related to any real signal.\n",
    "\n",
    "Most of these are fundamental characteristics of the two methods, which we'll consider below.\n",
    "\n",
    "# Periodogram Estimation\n",
    "\n",
    "The idea of periodogram estimation is that the [Weiner-Khinchin Theorem](https://en.wikipedia.org/wiki/Wiener%E2%80%93Khinchin_theorem) (spellings vary) tells us that the power spectral density for a wide-sense stationary process is the Fourier transform of the autocorrelation function of the data.  That is:\n",
    "\n",
    "$$S(f) = \\int_{-\\infty}^{\\infty} r_{xx}(\\tau)e^{-j (2\\pi)f\\tau}d\\tau$$\n",
    "\n",
    "where $r_{xx}(\\tau)$ is given by:\n",
    "\n",
    "$$r_{xx}(\\tau) = \\mathbb{E}[x(t)^* x(t - \\tau)]$$\n",
    "\n",
    "(i.e., the expected value of the product of the complex conjugate and itself, shifted by $\\tau$).  A correlation function measures the similarity between two signals when they are shifted in time relative to each other for various offsets.  Intuitively, if a signal is lined without a shift, the product in the expectation above is always positive, and therefore the expectation is also positive.  However, as $\\tau$ varies, it's likely that there will be some positions where the unshifted and shifted signals have different signs, and therefore part of the expectation integral is going to be negative, leading to a smaller value that the $\\tau=0$ version.  The correlation function can therefore be said to measure similarity between the two signals, or self-similarity in this case.\n",
    "\n",
    "In practice, we of course compute a discrete-time (digital) equivalent to the autocorrelation function:\n",
    "\n",
    "$$r_{xx}[k] = \\mathbb{E}\\left[ x[n]^* x[n-k]\\right]$$\n",
    "\n",
    "and then Discrete Fourier Transform, but the principle is the same.  Computing the autocorrelation with `scipy.signal.correlate()` shows intuitively why this works:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sd_samples = noise_model.make_wave(duration=sample_duration, framerate=samp_freq)\n",
    "noise_acf = signal.correlate(sd_samples.ys, sd_samples.ys) / len(sd_samples.ys)\n",
    "signal_acf = signal.correlate(input_samples.ys, input_samples.ys) / len(input_samples.ys)\n",
    "\n",
    "disp.display_acfs([noise_acf, signal_acf], ['Pure Noise', 'Signal + Noise'], samp_freq,\n",
    "                  arange=(-2.0, 2.0), trange=(-0.01, 0.01))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For the pure noise signal, the only significant value is when the signal is not shifted (i.e., $\\tau = 0$, which is true for all signals: they always match themselves best when not shifted.  However, for pure noise, any shift leads to equal numbers of pairs (i.e., $x[n]^*x[n-k]$) that are positive and negative, so they cancel out on average: all non-zero offsets are almost all zero (within computational noise).\n",
    "\n",
    "For the signal with the periodicity, however, the situation is quite different.  There's still a large spike at zero lag, but now there's a periodic signal at other lags, and it has the same wavelength as the periodic signal: one wavelength is 0.0025s, or 400Hz.  This isn't a coincidence, as you might expect: when the signals are shifted by exactly a wavelength of the periodic signal, the periodic parts again line up and give more positive pairs $x[n]^*x[n-k]$ in the expectation sum, giving the peaks; shifted by half a wavelength, more of the pairs are negative, giving the troughs.  If you have multiple periodicities in the signal, you'd see a summation of all of them in the autocorrelation function.\n",
    "\n",
    "This, then, is the thing that we Fourier transform, and it should be clear that this periodic signal will show up as a spike in the spectrum - the method transforms a very noisy signal into a less noisy signal with the same frequency, making the estimation problem significantly simpler.\n",
    "\n",
    "# Welch-Bartlett Averaged Periodogram\n",
    "\n",
    "The simplest version of the periodogram, above, is clearly very noisy.  Unfortunately, this is a characteristic of the computation: it doesn't matter how much data we get, the behavior is the same.  In order for the estimate to be useful, therefore, we need to do something else.\n",
    "\n",
    "The most common alternative approach is that proposed by [Bartlett](https://en.wikipedia.org/wiki/M._S._Bartlett), and adjusted by P. D. Welch, and now known as [Welch's Method](https://en.wikipedia.org/wiki/Welch%27s_method), or the Welch-Bartlett Averaged Periodogram.  The basic principle is to split the signal data into shorter segments, which may overlap (often the default is 50% overlap between adjacent segments), and then compute the periodogram for each segment, all of which are subsequently averaged in the frequency domain to reduce the noise.  For extra sidelobe supression (as with doing windowed FIR filter design), each segment can be multiplied by a suitable window before computing the periodogram (a [Hann Window](https://en.wikipedia.org/wiki/Hann_function) or [Hamming Window](https://en.wikipedia.org/wiki/Window_function#Hamming_window) is often used, but any suitable window function is allowed in principle).\n",
    "\n",
    "The effects of averaging are to suppress the estimation noise in the spectrum where there isn't a signal, and to build up the signal where there is.  There are some effects of the parameters chosen, but in general, the variability of the spectrum is significantly reduced:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "welch_f, welch_Pxx = signal.welch(input_samples.ys, fs=samp_freq, nperseg=4096)\n",
    "welch = (welch_f, welch_Pxx)\n",
    "disp.display_power_spectra([per, welch], ['Periodogram', 'Welch Periodogram'],\n",
    "                           mrange=(-75, 0))\n",
    "disp.display_power_spectra([per, welch], ['Periodogram', 'Welch Periodogram'],\n",
    "                           frange=(sig_freq-100, sig_freq+100), mrange=(-75, 0))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There is, of course, a trade-off.  Breaking the signal into smaller segments changes the achievable resolution of the signal, and using a window function causes the same increase in mainlobe width that we saw looking at windowed FIR filter design (i.e., that the transition from pass-band to stop-band takes longer).  Here, it shows up as a slightly wider peak at the signal frequency.\n",
    "\n",
    "This caveat in smoothing out the spectrum variability has an important consequence for the ability to separate closely-spaced frequency components.  That is, as the segments get shorter, the resolution of the periodogram decreases, and peaks that are narrowly separated start to appear to merge together.  For example, if we add a second signal at 10Hz from the first:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sig2_model = dsp.CosSignal(freq=sig_freq+10, amp=signal_amp, offset=0.0)\n",
    "sig2_samples = sig2_model.make_wave(duration=sample_duration, framerate=samp_freq)\n",
    "\n",
    "input_model = signal_model + sig2_model + noise_model\n",
    "input_samples = input_model.make_wave(duration=sample_duration, framerate=samp_freq)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "and then analyze with different length windows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "analysis_lengths = [512, 1024, 2048, 4096]\n",
    "spectra = []\n",
    "for alen in analysis_lengths:\n",
    "    welch_f, welch_Pxx = signal.welch(input_samples.ys, fs=samp_freq, nperseg=alen,\n",
    "                                      nfft=analysis_lengths[-1])\n",
    "    spectra.append((welch_f, welch_Pxx))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "the results show that as the windows get shorter, we end up with only a single peak:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "disp.display_sig_and_psd([signal_samples, sig2_samples, input_samples], spectra,\n",
    "                         analysis_lengths,\n",
    "                         trange=(0, 0.05),\n",
    "                         frange=(sig_freq-100, sig_freq+100), mrange=(-40, 0))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "which could significantly alter the interpretation of the signal properties.  In setting up an analysis, it's important to know what the required properties of the estimation are, so that this sort of thing doesn't happen without knowing about it.\n",
    "\n",
    "# Parametric PSD Estimation\n",
    "\n",
    "The basic principle of parametric PSD estimation is that there is a model, defined in terms of a set of parameters, that has a known power spectral density.  If, then, we can adjust the parameters of the model to minimise the difference between the observed data and the model output, then the PSD of the model (which is easy to compute) has to be the same as the PSD of the data.  In effect, we bypass the periodogram computation by finding the model parameters, and then computing the theoretical PSD from them.\n",
    "\n",
    "A simple example of this is to use an IIR filter for the model, driven by uncorrelated Gaussian noise of fixed standard deviation.  The IIR filter has a known frequency response (the transfer function) which can be directly computed from the $a(i), b(i)$ parameters, and therefore it re-shapes the spectrum of any signal fed into it accordingly.  Uncorrelated Gaussian noise, however, has a \"white\" (as in light) spectrum, meaning that it has equal power all all frequencies (subject to estimation noise):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "welch_f, welch_Pxx = signal.welch(ergodic_signal.ys, fs=samp_freq, nperseg=4096)\n",
    "disp.display_sig_and_psd([ergodic_signal], [(welch_f, welch_Pxx)], [4096], trange=(0, 0.05))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "and therefore the PSD of the output of the IIR filter driven by this noise is just the shape of the filter's transfer function.\n",
    "\n",
    "## Parametric Estimation\n",
    "\n",
    "Consequently, if we put the filter into a feedback loop that adjusts the $a(i), b(i)$ parameters until the output of the filter matches the input signal, computing the transfer function gives us an estimate of the PSD for the input signal.  Variants include using just the $a(i)$ coefficients (an autoregresive (AR) model).\n",
    "\n",
    "By way of example, we can take a two-component signal at different frequencies, and sample sufficient to get enough samples:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "system_snr = 0.0 # Signal to noise ratio, dB\n",
    "sig_freqs = [1840.0, 2880.0] # Frequencies of the signals, Hertz\n",
    "samp_freq = 16000 # Sampling frequency, Hertz\n",
    "samp_duration = 2.0 # Duration of signal, seconds (must be enough to get 10,000 samples)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then, we can synthesize the signal and add noise to give us the [signal-to-noise ratio](https://en.wikipedia.org/wiki/Signal-to-noise_ratio) required:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "signal_model = None\n",
    "for s in range(len(sig_freqs)):\n",
    "    if s == 0:\n",
    "        signal_model = dsp.CosSignal(freq=sig_freqs[s], amp=1.0, offset=0.0)\n",
    "    else:\n",
    "        signal_model += dsp.CosSignal(freq=sig_freqs[s], amp=1.0, offset=0.0)\n",
    "\n",
    "signal_samples = signal_model.make_wave(duration=samp_duration, framerate=samp_freq)\n",
    "\n",
    "noise_stdev = 10**(-system_snr/20.0)/np.sqrt(2.0)\n",
    "print('Noise standard deviation = %.3f' % (noise_stdev,))\n",
    "\n",
    "noise_model = dsp.UncorrelatedGaussianNoise(amp=noise_stdev)\n",
    "noise_samples = noise_model.make_wave(duration=samp_duration, framerate=samp_freq)\n",
    "\n",
    "input_samples = noise_samples + signal_samples\n",
    "\n",
    "disp.display_signal([signal_samples, input_samples], ['Noise Free', 'Input'],\n",
    "                    trange=(0, 0.05))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we take 10,000 samples (a long signal), we can compute the Welch periodogram as a reference, and an AR model with 12 coefficients as a parametric estimate:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "welch_f, welch_Pxx = signal.welch(input_samples.ys[0:10000], fs=samp_freq, nperseg=2048)\n",
    "input_psd = (welch_f, welch_Pxx)\n",
    "ar_model = pyule(input_samples.ys[0:10000], 12, sampling=samp_freq, scale_by_freq=False)\n",
    "ar_psd = (ar_model.frequencies(), ar_model.psd)\n",
    "\n",
    "disp.display_power_spectra([input_psd, ar_psd], ['Welch Periodogram', 'AR(12) Parametric'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see that the peaks in the AR model match the locations of the peaks from the Welch periodogram, although they're a little wider and a little lower.  Remember, however, that this is a power spectral **density**, so the power is the area under the curve, not the peak value.  Note how the AR model PSD is significantly smoother than the Welch PSD, which is characteristic of parametric methods (and one of the advantages).\n",
    "\n",
    "Where parametric methods really shine, however, is with short signal sequences.  Remember that we're only approximating ergodic signals, so the shorter we can make the signal sequence, the better able we'll be to follow changes in the signal, and the fewer assumptions we need to make.\n",
    "\n",
    "For example, consider a signal of 100 samples, and compute a Welch periodogram with eight segments, and an AR model with 12 parameters:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "welch_f, welch_Pxx = signal.welch(input_samples.ys[0:100], fs=samp_freq,\n",
    "                                  nperseg=15, nfft=2048)\n",
    "short_input_psd = (welch_f, welch_Pxx)\n",
    "\n",
    "short_ar_model = pyule(input_samples.ys[0:100], 12, sampling=samp_freq, NFFT=2048,\n",
    "                       scale_by_freq=False)\n",
    "short_ar_psd = (short_ar_model.frequencies(), short_ar_model.psd)\n",
    "\n",
    "disp.display_power_spectra([input_psd, short_input_psd, short_ar_psd],\n",
    "                           ['Full Signal Welch', 'Short Signal Welch', 'Short Signal AR(12)'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here, with reference to the full-signal Welch periodogram, we can see that the AR model still finds two separate peaks, and is in fact essentially the same as before.  The short-signal Welch periodogram, however, although it finds increased power density at the right area does not have sufficient frequency resolution to distinguish the peaks.\n",
    "\n",
    "## Model Order Estimation\n",
    "\n",
    "Although the estimation code finds the right filter parameters, the code above arbitrarily chose 12 coefficients to set up the filter.  The PSD that we get, however, depends on the number of coefficients that we allow for the filter.\n",
    "\n",
    "To see this, set up the signal again (although with better SNR this time around to make the estimation a little clearer):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "system_snr = 20.0\n",
    "signal_model = None\n",
    "for s in range(len(sig_freqs)):\n",
    "    if s == 0:\n",
    "        signal_model = dsp.CosSignal(freq=sig_freqs[s], amp=1.0, offset=0.0)\n",
    "    else:\n",
    "        signal_model += dsp.CosSignal(freq=sig_freqs[s], amp=1.0, offset=0.0)\n",
    "\n",
    "signal_samples = signal_model.make_wave(duration=samp_duration, framerate=samp_freq)\n",
    "\n",
    "noise_stdev = 10**(-system_snr/20.0)/np.sqrt(2.0)\n",
    "print('Noise standard deviation = %.3f' % (noise_stdev,))\n",
    "\n",
    "noise_model = dsp.UncorrelatedGaussianNoise(amp=noise_stdev)\n",
    "noise_samples = noise_model.make_wave(duration=samp_duration, framerate=samp_freq)\n",
    "\n",
    "input_samples = noise_samples + signal_samples\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "and then compute the PSD using the same method as before (the `aryule()` function does the same computation, but provides more detailed output information that we need later) over a range of model orders (number of coefficients), still using 100 samples, plotting the results:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "order = np.arange(2, 22, step=2)\n",
    "psds = []\n",
    "psd_names = []\n",
    "residuals = []\n",
    "for n in order:\n",
    "    ar, rho, _ = aryule(input_samples.ys[0:100], n, norm='biased')\n",
    "    psd = arma2psd(ar, rho=rho, T=1.0/samp_freq)\n",
    "    psds.append((np.arange(0, len(psd))*(samp_freq/len(psd)), psd))\n",
    "    psd_names.append('Order = %d' % (n,))\n",
    "    residuals.append(rho)\n",
    "\n",
    "disp.display_power_spectra(psds, psd_names, frange=(0, samp_freq/2.0))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For order 2 and 4, the estimator fails to distinguish between the two peaks from the different components, which is problematic.  For higher orders, we start to see two peaks, and for higher orders we get better separation between the two signals (the valley between gets lower), although we also start to get other responses at different frequencies, which can potentially be misinterpreted.\n",
    "\n",
    "So there's a question of how to estimate the right model order to use for fitting the AR model.  In general, we need at least two coefficients for each frequency component in the input signal, so knowing that there are two components here explains why order 2 and 4 models fail to succeed in detecting both components.  Clearly, however, if we keep adding coefficients, we run the risk of over-fitting the data, leading to spurious peaks that can be misinterpreted.  So we're really looking for a \"just right\" number of coefficients.\n",
    "\n",
    "There are a number of ways to do this, but a simple method is to look at the $\\rho$ (`rho`) coefficient output by the `aryule()` function, which represents the variance of the residual noise in the signal not explained by the filter itself.  In general, this reduces the more coefficients that we add:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "disp.display_signal([(order, residuals)], ['Residual Uncertainty'], trange=(2, 20),\n",
    "                    xlabel='AR Model Order', ylabel='Noise Variance')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "which suggests that we should add coefficients until the variance reduces to a plateau level, and then stop.  In this case, it seems likely that at about 10-12 coefficients, adding more doesn't help significantly.\n",
    "\n",
    "# Other Things To Try\n",
    "\n",
    "The presentation of non-parametric and parametric methods above is necessarily light: there are many other methods and parameter choices that could be exploited.  In parametric methods, particularly, there are a number of different approaches to the AR modeling problem, and a number of different techniques that, for example, rely on eigen-analysis of the autocorrelation matrix of the signal, or which achieve super-resolution through [significantly more complex computation](https://en.wikipedia.org/wiki/MUSIC_(algorithm)).  Many of these methods would benefit from further investigation, including:\n",
    "1. The Blackman-Tukey periodogram averages in frequency rather than time, and behaves differently to the Welch periodogram.\n",
    "2. [Multi-taper Methods](https://en.wikipedia.org/wiki/Multitaper) are an alternative approach to non-parametric PSD estimation which attempts to avoid some of the biases that occur otherwise.\n",
    "3. [Pisarenko's Method](https://en.wikipedia.org/wiki/Pisarenko_harmonic_decomposition) considers an eigen-decomposition of the autocorrelation matrix, separating out the eigenvectors that appear to be noise so they can be removed; what remains estimates the signal's PSD.\n",
    "4. [MUSIC](https://en.wikipedia.org/wiki/MUSIC_(algorithm)), the Multiple Signal Classification algorithm also implements eigen-decomposition of the autocorrelation matrix, and is similar to Pisarenko's Method with smoothing to improve performance.\n",
    "5. [ESPRIT](https://en.wikipedia.org/wiki/Estimation_of_signal_parameters_via_rotational_invariance_techniques), \"Estimation of Signal Parameters via Rotational Invariance Techniques\", can be used to estimate frequencies, but is also used for angle-of-arrival estimation in phase arrays.  The algorithm uses a singular value decomposition (SVD) of the signal covariance matrix to identify which eigenvectors represent signal, and which noise.\n",
    "\n",
    "# Copyright and License\n",
    "\n",
    "Copyright 2019, University of New Hampshire, Center for Coastal and Ocean Mapping.\n",
    "\n",
    "This program is free software: you can redistribute it and/or modify\n",
    "it under the terms of the GNU General Public License as published by\n",
    "the Free Software Foundation, either version 3 of the License, or\n",
    "(at your option) any later version.\n",
    "\n",
    "This program is distributed in the hope that it will be useful,\n",
    "but WITHOUT ANY WARRANTY; without even the implied warranty of\n",
    "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n",
    "GNU General Public License for more details.\n",
    "\n",
    "You should have received [a copy of the GNU General Public License](COPYING)\n",
    "along with this program.  If not, see [here](https://www.gnu.org/licenses/)."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
