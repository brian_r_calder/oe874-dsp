#   Copyright 2019, University of New Hampshire, Center for Coastal and Ocean Mapping.
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

import matplotlib.pyplot as plt
import thinkdsp as dsp
import dsp_aux as aux


def make_spectra(f, lofi_fs, hifi_fs, **kwargs):
    signal = dsp.SinSignal(freq=f, amp=1.0, offset=0.0)
    wave_lofi = signal.make_wave(duration=1.0, framerate=lofi_fs)
    wave_hifi = signal.make_wave(duration=1.0, framerate=hifi_fs)
    spectrum_lofi = wave_lofi.make_spectrum(full=True)
    spectrum_hifi = wave_hifi.make_spectrum(full=True)

    return spectrum_lofi, spectrum_hifi


def spectrum_comparison(lofi_spectrum, hifi_spectrum, **kwargs):
    def stdaxis(fs, freq_range):
        plt.axis((-freq_range / 2.0, freq_range / 2.0, -0.05, 0.55))
        plt.grid()
        plt.ylabel('Magnitude')
        plt.title('Sampling Frequency %.1f Hz' % (fs,))

    plt.figure(figsize=(14, 10))

    plt.subplot(2, 1, 1)
    plt.stem(lofi_spectrum.fs, lofi_spectrum.amps / len(lofi_spectrum.amps), use_line_collection=True)
    stdaxis(lofi_spectrum.framerate, hifi_spectrum.framerate)

    plt.subplot(2, 1, 2)
    plt.stem(hifi_spectrum.fs, hifi_spectrum.amps / len(hifi_spectrum.amps), use_line_collection=True)
    stdaxis(hifi_spectrum.framerate, hifi_spectrum.framerate)
    plt.xlabel('Frequency (Hz)')

    if 'save' in kwargs:
        aux.save_figure(kwargs['save'])

    plt.show()
