#   Copyright 2019, University of New Hampshire, Center for Coastal and Ocean Mapping.
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

import thinkdsp as dsp
import square_wave_smoothing_plots as disp
import numpy as np

signal = dsp.SquareSignal(freq=1.0, amp=1.0, offset=np.pi)
wave = signal.make_wave(duration=5.0, framerate=100)

disp.plot_time_and_spectrum(wave, 'Original Signal', save='square_wave_and_spectrum')

smoothing = [2, 5, 10, 20]
for s in smoothing:
    smoothed = wave.convolve((1.0/s)*np.ones(s))
    if s != smoothing[-1]:
        disp.plot_time_and_spectrum(smoothed, 'Smoothing Length = %d.' % (s,))
    else:
        disp.plot_time_and_spectrum(smoothed, 'Smoothing Length = %d.' % (s,), save='smoothed_square_wave')
