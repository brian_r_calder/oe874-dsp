#   Copyright 2019, University of New Hampshire, Center for Coastal and Ocean Mapping.
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

import matplotlib.pyplot as plt
import numpy as np
import thinkdsp as dsp
import dsp_aux as aux


def display_signal(waves, names, **kwargs):
    if 'show' in kwargs:
        show = kwargs['show']
    else:
        show = True

    if 'ylabel' in kwargs:
        vert_label = kwargs['ylabel']
    else:
        vert_label = 'Signal Amplitude'

    if 'xlabel' in kwargs:
        horz_label = kwargs['xlabel']
    else:
        horz_label = 'Time (s)'

    plt.figure(figsize=(14,10))
    n_subplots = len(waves)
    for w in range(len(waves)):
        plt.subplot(n_subplots, 1, w+1)
        if isinstance(waves[w], dsp.Wave):
            plt.plot(waves[w].ts, waves[w].ys)
        else:
            if isinstance(waves[w], tuple):
                plt.plot(waves[w][0], waves[w][1])
            else:
                sig_len = len(waves[w])
                plt.plot(np.linspace(-sig_len/2.0, sig_len/2.0, num=sig_len), waves[w])

        plt.grid()
        plt.legend((names[w],))
        plt.xlabel(horz_label)
        plt.ylabel(vert_label)
        if 'trange' in kwargs:
            plt.gca().set(xlim=kwargs['trange'])
        if 'srange' in kwargs:
            plt.gca().set(ylim=kwargs['srange'])

    if 'save' in kwargs:
        aux.save_figure(kwargs['save'])

    if show:
        plt.show()


def display_acfs(acfs, names, fs, **kwargs):
    if 'show' in kwargs:
        show = kwargs['show']
    else:
        show = True

    plt.figure(figsize=(14,10))
    n_subplots = len(acfs)
    for w in range(len(acfs)):
        plt.subplot(n_subplots, 1, w+1)
        if isinstance(acfs[w], dsp.Wave):
            plt.plot(acfs[w].ts, acfs[w].ys)
        else:
            sig_len = len(acfs[w])

            plt.plot(np.linspace(-(sig_len/fs)/2.0, (sig_len/fs)/2.0, num=sig_len), acfs[w])

        plt.grid()
        plt.legend((names[w],))
        plt.xlabel('Time Shift (s)')
        plt.ylabel('Autocorrelation Estimate')
        if 'trange' in kwargs:
            plt.gca().set(xlim=kwargs['trange'])
        if 'arange' in kwargs:
            plt.gca().set(ylim=kwargs['arange'])

    if 'save' in kwargs:
        aux.save_figure(kwargs['save'])

    if show:
        plt.show()


def display_power_spectra(signals, names, **kwargs):
    if 'show' in kwargs:
        show = kwargs['show']
    else:
        show = True

    plt.figure(figsize=(14,10))
    for s in range(len(signals)):
        if isinstance(signals[s], tuple):
            plt.plot(signals[s][0], 10.0 * np.log10(abs(signals[s][1])))
        else:
            plt.plot(signals[s].frequencies(), 10.0 * np.log10(abs(signals[s].psd)))

    if 'frange' in kwargs:
        plt.gca().set(xlim=kwargs['frange'])
    else:
        if isinstance(signals[0], tuple):
            plt.gca().set(xlim=(0, signals[0][0][-1]))
        else:
            plt.gca().set(xlim=(0, signals[0].frequencies()[-1]))

    if 'mrange' in kwargs:
        plt.gca().set(ylim=kwargs['mrange'])

    plt.legend(names)

    plt.grid()
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Power Density (dB/Hz)')

    if 'save' in kwargs:
        aux.save_figure(kwargs['save'])

    if show:
        plt.show()


def display_sig_and_psd(signals, psds, analysis_lengths, **kwargs):
    if 'show' in kwargs:
        show = kwargs['show']
    else:
        show = True

    plt.figure(figsize=(14,10))
    plt.subplot(2, 1, 1)
    for s in signals:
        plt.plot(s.ts, s.ys)

    plt.grid()
    plt.xlabel('Time (s)')
    plt.ylabel('Signal Amplitude')

    if 'trange' in kwargs:
        plt.gca().set(xlim=kwargs['trange'])
    if 'srange' in kwargs:
        plt.gca().set(ylim=kwargs['srange'])

    plt.subplot(2, 1, 2)
    legtext = []
    for p in range(len(psds)):
        plt.plot(psds[p][0], 10.0*np.log10(abs(psds[p][1])))
        legtext.append('Analysis Window = %d samples' % (analysis_lengths[p],))

    if 'frange' in kwargs:
        plt.gca().set(xlim=kwargs['frange'])
    else:
        if isinstance(psds[0], tuple):
            plt.gca().set(xlim=(0, psds[0][0][-1]))
        else:
            plt.gca().set(xlim=(0, psds[0].frequencies()[-1]))

    if 'mrange' in kwargs:
        plt.gca().set(ylim=kwargs['mrange'])

    plt.grid()
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Power Density (dB/Hz)')
    plt.legend(legtext)

    if 'save' in kwargs:
        aux.save_figure(kwargs['save'])

    if show:
        plt.show()
