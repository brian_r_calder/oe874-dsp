#   Copyright 2019, University of New Hampshire, Center for Coastal and Ocean Mapping.
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

import thinkdsp as dsp
import subprocess
import matplotlib.pyplot as plt
import os


def play_audio(signal):
    """ Plays a ThinkDSP.Wave object with VLC by saving first as WAVE file.  Note that
        your mileage may vary considerably depending on OS you're using.  This by default
        works with VLC on macOS.
        
        signal: ThinkDSP.Wave
    """
    if not isinstance(signal, dsp.Wave):
        print('Need a ThinkDSP.Wave to play.')
        return
    signal.write('sound.wav')
    cmd = 'open sound.wav'
    popen = subprocess.Popen(cmd, shell=True)
    popen.communicate()


def make_note(freq, samp_freq=8000, duration=3.0):
    """ Generates and then plays a note of given frequency, sampled at the
        specified frequency, for the specified duration.
        
        freq: frequency of note, Hz
        samp_freq: sampling frequency, Hz
        duration: time to play, s
    """
    sig = dsp.SinSignal(freq=freq, amp=0.1, offset=0.0)
    wave = sig.make_wave(duration=duration, framerate=samp_freq)
    return wave


def make_chord(freqs, samp_freq=8000, duration=3.0):
    """ Generate and play a small sample of the given list of frequencies, added
        together, at specified sampling frequency and duration.
        
        freqs:  list of frequencies, Hz
        samp_freq: sampling frequency, Hz
        duration: time to play, s
    """
    n_freqs = len(freqs)
    signal = None
    for s in range(n_freqs):
        sig = dsp.SinSignal(freq=freqs[s], amp = 0.1/n_freqs, offset = 0)
        if signal is None:
            signal = sig
        else:
            signal += sig
    wave = signal.make_wave(duration=duration, framerate=samp_freq)

    return wave


def save_figure(basename):
    if not os.path.exists('figures'):
        os.makedirs('figures')
    plt.savefig('figures/' + basename + '.png', dpi=150, format='png')
    plt.savefig('figures/' + basename + '.pdf', format='pdf')


def make_auxdata_dir():
    # We need to check that the auxdata directory exists so that we can output the processed files
    if not os.path.exists('auxdata'):
        os.makedirs('auxdata')
    else:
        if not os.path.isdir('auxdata'):
            print('Error: an "auxdata" exists, and is not a directory!')
            exit(1)
