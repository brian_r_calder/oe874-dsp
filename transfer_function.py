#   Copyright 2019, University of New Hampshire, Center for Coastal and Ocean Mapping.
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

import thinkdsp as dsp
from scipy import signal
import transfer_function_plots as disp
import matplotlib.pyplot as plt

# We're trying to build up the relationship between the impulse response and the transfer function.
# To do so, generate two different signals 200Hz above/below the corner frequency for a filter, then
# filter, and display the results, and the transfer function of the filter.

signal_freq = (400, 800)
samp_freq = 8000

input_samples = []
for s in signal_freq:
    input_signal = dsp.CosSignal(freq=s, amp=1.0, offset=0.0)
    input_samples.append(input_signal.make_wave(duration=1.0, framerate=samp_freq))

filter_length = 51
corner_freq = 600

filt_b = signal.firwin(filter_length, corner_freq, window='blackmanharris', fs=samp_freq)

output_samples = []
for samples in input_samples:
    output_samples.append(samples.convolve(filt_b))
    
disp.display_timeplot(input_samples, output_samples, trange=(0.01, 0.02), save='transfer_function_timeplot')

f, H = signal.freqz(filt_b, fs=samp_freq)
disp.display_transfer_function(f, H, save='transfer_function_freqz')

plt.show()
