#   Copyright 2019, University of New Hampshire, Center for Coastal and Ocean Mapping.
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

import thinkdsp as dsp
import dsp_aux as aux
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal
import iir_filter_design_plots as disp

sampling_frequency = 8000.0 # Hertz
corner_frequency = 2000.0   # Hertz

# Generate example low-pass IIR filters at the same corner frequency
filter_order = 8

feedback_coeffs = []
input_coeffs = []
filter_names = []

b, a = signal.butter(filter_order, corner_frequency, fs = sampling_frequency)
input_coeffs.append(b)
feedback_coeffs.append(a)
filter_names.append('Butterworth')

b, a = signal.cheby1(filter_order, 5.0, corner_frequency, fs = sampling_frequency)
input_coeffs.append(b)
feedback_coeffs.append(a)
filter_names.append('Chebychev Type-I, 5.0dB Ripple')

b, a = signal.ellip(filter_order, 5.0, 60.0, corner_frequency,fs = sampling_frequency)
input_coeffs.append(b)
feedback_coeffs.append(a)
filter_names.append('Elliptical 5.0dB Ripple, 60dB Cut')

disp.display_iir_filters(input_coeffs, feedback_coeffs, sampling_frequency, title=filter_names, save='iir-coeffs')
disp.display_iir_spectra(input_coeffs, feedback_coeffs, sampling_frequency, legend=filter_names, logrange=(-100, 5), save='iir-spectra')

# Give comparison of an FIR and IIR for the same corner frequency in order to
# show the difference in phase and amplitude response
feedback_coeffs = []
input_coeffs = []
filter_names = []

fir_filter_order = 31
b = signal.firwin(fir_filter_order, corner_frequency, window='blackmanharris', fs=sampling_frequency)
a = np.ones(1)
feedback_coeffs.append(a)
input_coeffs.append(b)
filter_names.append('Blackman-Harris FIR N = %d' % (fir_filter_order,))

b, a = signal.butter(filter_order, corner_frequency, fs=sampling_frequency)
feedback_coeffs.append(a)
input_coeffs.append(b)
filter_names.append('Butterworth IIR N = %d' % (filter_order,))

disp.display_spectra_comparison(input_coeffs, feedback_coeffs, sampling_frequency, filter_names, logrange=(-100, 5), save='filter-spectra-comparison')

# Demonstrate the difference between FIR and IIR on a square wave (effects of
# phase)

filter_order = 10 # Use the same length for both filters
signal_frequency = 400.0 # Square wave frequency, Hz

feedback_coeffs = []
input_coeffs = []
filter_names = []

b = signal.firwin(filter_order, corner_frequency, window='blackmanharris', fs=sampling_frequency)
input_coeffs.append(b)
feedback_coeffs.append(np.ones(1))
filter_names.append('Blackman-Harris FIR N = %d' % (filter_order,))

b, a = signal.butter(filter_order, corner_frequency, fs=sampling_frequency)
input_coeffs.append(b)
feedback_coeffs.append(a)
filter_names.append('Butterworth IIR N = %d' % (filter_order,))

disp.display_spectra_comparison(input_coeffs, feedback_coeffs, sampling_frequency, filter_names, logrange=(-100, 5), save='fir-iir-filter-comparison-spectra')

source = dsp.SquareSignal(freq=signal_frequency, amp=1.0)
wave = source.make_wave(framerate=sampling_frequency)

filtered_signals = []
signal_names = []

filtered_signals.append(wave.ys)
signal_names.append('Original, Square %.2f Hz' % (signal_frequency,))

for f in range(len(filter_names)):
    filtered = signal.lfilter(input_coeffs[f], feedback_coeffs[f], wave.ys)
    filtered_signals.append(filtered)
    signal_names.append('Filtered: ' + filter_names[f])

disp.display_wave_comparison(wave.ts, filtered_signals, signal_names, save='filtered-signal-comparison')

# Finally, make a set of filters for the hydrophone filtering with IIR rather
# than FIR
record = dsp.read_wave('hydrophone.wav')

sampling_frequency = record.framerate
iir_filter_length = 10
fir_filter_length = 201

filter_spec = []
filter_names = []

corner_freq = 2500.0
b = signal.firwin(fir_filter_length, corner_freq, window='blackmanharris', fs=sampling_frequency, pass_zero=False)
z, p, k = signal.tf2zpk(b, np.ones(1))
filter_spec.append((z, p, k))
filter_names.append('FIR Ship Suppression')

corner_freq = 1165.0
z, p, k = signal.cheby2(iir_filter_length, 120.0, corner_freq, btype='highpass', fs=sampling_frequency, output='zpk')
filter_spec.append((z, p, k))
filter_names.append('IIR Ship Suppression')

corner_freq = [9500.0, 15500.0]
b = signal.firwin(fir_filter_length, corner_freq, window='blackmanharris', fs=sampling_frequency, pass_zero=False)
z, p, k = signal.tf2zpk(b, np.ones(1))
filter_spec.append((z, p, k))
filter_names.append('FIR MBES Bandpass')

corner_freq = [7300.0, 19460.0]
z, p, k = signal.cheby2(iir_filter_length, 120.0, corner_freq, btype='bandpass', fs=sampling_frequency, output='zpk')
filter_spec.append((z, p, k))
filter_names.append('IIR MBES Bandpass')

corner_freq = [2500.0, 6500.0]
b = signal.firwin(fir_filter_length, corner_freq, window='blackmanharris', fs=sampling_frequency, pass_zero=False)
z, p, k = signal.tf2zpk(b, np.ones(1))
filter_spec.append((z, p, k))
filter_names.append('FIR SBP Bandpass')

corner_freq = [1600.0, 10000.0]
z, p, k = signal.cheby2(iir_filter_length, 120.0, corner_freq, btype='bandpass', fs=sampling_frequency, output='zpk')
filter_spec.append((z, p, k))
filter_names.append('IIR SBP Bandpass')

disp.display_paired_spectra_comparison(filter_spec, sampling_frequency, filter_names, logrange =(-200, 5), save='hydrophone-filter-comparison-spectra')

# Ensure that we have a space in which to write the filtered audio data.
aux.make_auxdata_dir()

# Filter the hydrophone signal with each of the IIR filters
hpf = signal.zpk2sos(filter_spec[1][0], filter_spec[1][1], filter_spec[1][2])
sig_hp = signal.sosfilt(hpf, record.ys)
sig_wave = dsp.Wave(sig_hp, framerate = record.framerate)
disp.show_spectrogram(sig_wave, save='iir_highpass_hydrophone_spectrogram')
sig_wave.write('auxdata/iir_hp_hydrophone.wav')

mbes_f = signal.zpk2sos(filter_spec[3][0], filter_spec[3][1], filter_spec[3][2])
sig_mbes = signal.sosfilt(mbes_f, record.ys)
sig_wave = dsp.Wave(sig_mbes, framerate = record.framerate)
disp.show_spectrogram(sig_wave, save='iir_mbes_hydrophone_spectrogram')
sig_wave.write('auxdata/iir_mbes_hydrophone.wav')

sbp_f = signal.zpk2sos(filter_spec[5][0], filter_spec[5][1], filter_spec[5][2])
sig_sbp = signal.sosfilt(sbp_f, record.ys)
sig_wave = dsp.Wave(sig_sbp, framerate = record.framerate)
disp.show_spectrogram(sig_wave, save='iir_sbp_hydrophone_spectrogram')
sig_wave.write('auxdata/iir_sbp_hydrophone.wav')

# Demonstrate numerical instability in estimating IIR filters
filter_spec = filter_spec[4:]
filter_names = filter_names[4:]
corner_freq = [1600.0, 10000.0]
b, a = signal.cheby2(iir_filter_length, 120.0, corner_freq, btype='bandpass', fs=sampling_frequency)
z, p, k = signal.tf2zpk(b, a)
filter_spec.append((z, p, k))
filter_names.append('Bad IIR SBP Bandpass')

disp.display_filter_comparison(filter_spec, sampling_frequency, filter_names, logrange=(-200, 5), save='iir_filter_instability')

plt.show()
