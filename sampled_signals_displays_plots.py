#   Copyright 2019, University of New Hampshire, Center for Coastal and Ocean Mapping.
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

import matplotlib.pyplot as plt
from scipy import interpolate
import numpy as np
import dsp_aux as aux


def display_reconstructions(data, **kwargs):
    def stdaxis():
        plt.grid()
        plt.axis((0, 2, -1.1, 1.1))
        plt.ylabel('Amplitude')

    plt.figure(figsize=(14,10))

    plt.subplot(3,1,1)
    plt.stem(data.ts, data.ys, use_line_collection=True)
    stdaxis()

    plt.subplot(3,1,2)
    interpolator = interpolate.interp1d(data.ts, data.ys, kind='previous')
    ti = np.arange(data.start, data.end, step=(data.end-data.start)/1000)
    plt.plot(ti, interpolator(ti))
    stdaxis()

    plt.subplot(3,1,3)
    plt.plot(data.ts, data.ys)
    stdaxis()
    plt.xlabel('Time (s)')

    if 'save' in kwargs:
        aux.save_figure(kwargs['save'])

    plt.show()
